package com.epam.psychologicalassistanceservice.web.question.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.service.question.QuestionService;
import com.epam.psychologicalassistanceservice.web.question.domain.QuestionDto;
import com.epam.psychologicalassistanceservice.web.question.transformer.QuestionDtoTransformer;

@RestController
public class QuestionController {
    private static final String GET_QUESTION_MAPPING = "/api/question/{id}";
    private static final String ADD_QUESTION_MAPPING = "/api/question";
    private static final String GET_QUESTIONS_MAPPING = "/questions";

    @Autowired
    private QuestionService questionService;

    @Autowired
    private QuestionDtoTransformer questionDtoTransformer;

    @RequestMapping(value = GET_QUESTION_MAPPING, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public QuestionDto getQuestion(@PathVariable int id) {
        return questionDtoTransformer.transform(questionService.getById(id));
    }

    @RequestMapping(value = ADD_QUESTION_MAPPING, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Question addQuestion(@RequestBody QuestionDto question) {
        return questionService.save(questionDtoTransformer.transform(question));
    }

    @RequestMapping(value = GET_QUESTIONS_MAPPING, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<QuestionDto> getQuestions() {
        return questionDtoTransformer.transformList(questionService.getAll());
    }

}
