package com.epam.psychologicalassistanceservice.web.emotioncorrelation.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Emotion;
import com.epam.psychologicalassistanceservice.domain.question.EmotionCorrelation;
import com.epam.psychologicalassistanceservice.web.emotioncorrelation.domain.EmotionCorrelationDto;

@Component
public class EmotionCorrelationDtoTransformer {
    public EmotionCorrelationDto transform(EmotionCorrelation emotionCorrelation) {
        return EmotionCorrelationDto.builder()
            .id(emotionCorrelation.getId())
            .emotion(emotionCorrelation.getEmotion().toString())
            .points(emotionCorrelation.getPoints())
            .build();
    }

    public List<EmotionCorrelationDto> transformList(List<EmotionCorrelation> emotionCorrelations){
        return emotionCorrelations.stream().map(this::transform).collect(Collectors.toList());
    }

    public EmotionCorrelation transform(EmotionCorrelationDto emotionCorrelationDto) {
        return EmotionCorrelation.builder()
            .id(emotionCorrelationDto.getId())
            .emotion(Emotion.valueOf(emotionCorrelationDto.getEmotion()))
            .points(emotionCorrelationDto.getPoints())
            .build();
    }

    public List<EmotionCorrelation> transformDtoList(List<EmotionCorrelationDto> emotionCorrelationDtos){
        return emotionCorrelationDtos.stream().map(this::transform).collect(Collectors.toList());
    }
}
