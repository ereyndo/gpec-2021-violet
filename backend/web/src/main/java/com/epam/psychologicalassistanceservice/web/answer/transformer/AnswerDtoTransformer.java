package com.epam.psychologicalassistanceservice.web.answer.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Answer;
import com.epam.psychologicalassistanceservice.web.answer.domain.AnswerDto;
import com.epam.psychologicalassistanceservice.web.emotioncorrelation.transformer.EmotionCorrelationDtoTransformer;

@Component
public class AnswerDtoTransformer {

    @Autowired
    private EmotionCorrelationDtoTransformer emotionCorrelationDtoTransformer;

    public AnswerDto transform(Answer answer){
        return AnswerDto.builder()
            .id(answer.getId())
            .emotionCorrelations(emotionCorrelationDtoTransformer.transformList(answer.getEmotionCorrelations()))
            .text(answer.getText())
            .build();
    }

    public List<AnswerDto> transformList(List<Answer> answers){
        return answers.stream().map(this::transform).collect(Collectors.toList());
    }

    public Answer transform(AnswerDto answerDto){
        return Answer.builder()
            .id(answerDto.getId())
            .emotionCorrelations(emotionCorrelationDtoTransformer.transformDtoList(answerDto.getEmotionCorrelations()))
            .text(answerDto.getText())
            .build();
    }

    public List<Answer> transformDtoList(List<AnswerDto> answerDtos){
        return answerDtos.stream().map(this::transform).collect(Collectors.toList());
    }
}
