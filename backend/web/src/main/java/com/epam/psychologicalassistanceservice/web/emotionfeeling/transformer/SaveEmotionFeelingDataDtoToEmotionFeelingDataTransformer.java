package com.epam.psychologicalassistanceservice.web.emotionfeeling.transformer;

import java.time.ZonedDateTime;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.domain.SaveEmotionFeelingDataDto;

@Component
public class SaveEmotionFeelingDataDtoToEmotionFeelingDataTransformer {
    public EmotionFeelingData transform(SaveEmotionFeelingDataDto saveEmotionFeelingDataDto) {
        return EmotionFeelingData.builder()
            .reasonOfFeeling(saveEmotionFeelingDataDto.getReasonOfFeeling())
            .emotions(saveEmotionFeelingDataDto.getEmotions())
            .build();
    }
}
