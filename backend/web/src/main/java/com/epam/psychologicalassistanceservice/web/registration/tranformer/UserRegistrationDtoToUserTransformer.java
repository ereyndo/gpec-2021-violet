package com.epam.psychologicalassistanceservice.web.registration.tranformer;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.web.registration.domain.UserRegistrationDto;

@Component
public class UserRegistrationDtoToUserTransformer {
    public User transform(UserRegistrationDto userRegistrationDto) {
        return User.builder()
            .email(userRegistrationDto.getEmail())
            .username(userRegistrationDto.getUsername())
            .password(userRegistrationDto.getPassword())
            .build();
    }
}
