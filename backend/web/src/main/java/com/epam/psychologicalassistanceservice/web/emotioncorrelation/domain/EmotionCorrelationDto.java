package com.epam.psychologicalassistanceservice.web.emotioncorrelation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EmotionCorrelationDto {
    private Integer id;
    private String emotion;
    private int points;
}
