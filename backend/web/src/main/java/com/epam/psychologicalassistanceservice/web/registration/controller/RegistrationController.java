package com.epam.psychologicalassistanceservice.web.registration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.psychologicalassistanceservice.service.user.UserService;
import com.epam.psychologicalassistanceservice.web.registration.domain.UserRegistrationDto;
import com.epam.psychologicalassistanceservice.web.registration.tranformer.UserRegistrationDtoToUserTransformer;

@RestController
public class RegistrationController {
    private static final String REGISTRATION_MAPPING = "/register";

    @Autowired
    private UserService userService;

    @Autowired
    private UserRegistrationDtoToUserTransformer toUserTransformer;

    @RequestMapping(value = REGISTRATION_MAPPING, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody UserRegistrationDto userRegistrationDto) {
        userService.save(toUserTransformer.transform(userRegistrationDto));
    }
}
