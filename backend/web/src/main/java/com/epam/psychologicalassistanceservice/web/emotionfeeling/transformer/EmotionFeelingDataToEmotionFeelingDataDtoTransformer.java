package com.epam.psychologicalassistanceservice.web.emotionfeeling.transformer;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.domain.EmotionFeelingDataDto;

@Component
public class EmotionFeelingDataToEmotionFeelingDataDtoTransformer {
    private static final String HUMAN_READABLE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private final DateTimeFormatter dateTimeFormatter;

    public EmotionFeelingDataToEmotionFeelingDataDtoTransformer() {
        this.dateTimeFormatter = DateTimeFormatter.ofPattern(HUMAN_READABLE_PATTERN);
    }

    public EmotionFeelingDataDto transform(EmotionFeelingData emotionFeelingData) {
        return EmotionFeelingDataDto.builder()
            .date(dateTimeFormatter.format(emotionFeelingData.getDate()))
            .emotions(emotionFeelingData.getEmotions())
            .reasonOfFeeling(emotionFeelingData.getReasonOfFeeling())
            .build();
    }

    public List<EmotionFeelingDataDto> transformMultiple(List<EmotionFeelingData> emotionFeelingDataList) {
        return emotionFeelingDataList.stream().map(this::transform).collect(Collectors.toList());
    }
}
