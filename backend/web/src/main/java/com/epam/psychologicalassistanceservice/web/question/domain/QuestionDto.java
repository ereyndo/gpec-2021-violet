package com.epam.psychologicalassistanceservice.web.question.domain;

import java.util.List;

import com.epam.psychologicalassistanceservice.web.answer.domain.AnswerDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class QuestionDto {
    private Integer id;
    private List<AnswerDto> answers;
    private String text;
}
