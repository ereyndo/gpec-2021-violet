package com.epam.psychologicalassistanceservice.web.emotionfeeling.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.service.emotionfeelingdata.EmotionFeelingService;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.domain.EmotionFeelingDataDto;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.domain.SaveEmotionFeelingDataDto;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.transformer.EmotionFeelingDataToEmotionFeelingDataDtoTransformer;
import com.epam.psychologicalassistanceservice.web.emotionfeeling.transformer.SaveEmotionFeelingDataDtoToEmotionFeelingDataTransformer;

@RestController
public class EmotionFeelingController {
    private static final String ADD_EMOTION_FEELING_DATA = "/moodfeelingtest";
    private static final String GET_CURRENT_USER_EMOTION_FEELING_DATA = "/moodfeelinghistory";
    private static final String GET_EMOTION_FEELING_DATA_BY_USER_ID = "/api/emotionfeelingdata/{id}";

    @Autowired
    private EmotionFeelingService emotionFeelingService;

    @Autowired
    private SaveEmotionFeelingDataDtoToEmotionFeelingDataTransformer saveTransformer;

    @Autowired
    private EmotionFeelingDataToEmotionFeelingDataDtoTransformer emotionFeelingDataTransformer;

    @RequestMapping(value = ADD_EMOTION_FEELING_DATA, method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public EmotionFeelingDataDto addPersonalEmotionFeelingData(@RequestBody SaveEmotionFeelingDataDto emotionFeelingData) {
        return emotionFeelingDataTransformer.transform(emotionFeelingService.saveOfCurrentUser(saveTransformer.transform(emotionFeelingData)));
    }

    @RequestMapping(value = GET_CURRENT_USER_EMOTION_FEELING_DATA, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<EmotionFeelingDataDto> getPersonalEmotionFeelingDataHistory() {
        return emotionFeelingDataTransformer.transformMultiple(emotionFeelingService.findAllOfCurrentUser());
    }

    @RequestMapping(value = GET_EMOTION_FEELING_DATA_BY_USER_ID, method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<EmotionFeelingDataDto> getEmotionFeelingData(@PathVariable String id) {
        return emotionFeelingDataTransformer.transformMultiple(emotionFeelingService.findAllByUserId(id));
    }
}
