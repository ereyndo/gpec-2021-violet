package com.epam.psychologicalassistanceservice.web.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.web.answer.transformer.AnswerDtoTransformer;
import com.epam.psychologicalassistanceservice.web.question.domain.QuestionDto;

@Component
public class QuestionDtoTransformer {

    @Autowired
    private AnswerDtoTransformer answerDtoTransformer;

    public QuestionDto transform(Question question){
        return QuestionDto.builder()
            .id(question.getId())
            .answers(answerDtoTransformer.transformList(question.getAnswers()))
            .text(question.getText())
            .build();
    }

    public List<QuestionDto> transformList(List<Question> questions){
        return questions.stream().map(this::transform).collect(Collectors.toList());
    }

    public Question transform(QuestionDto questionDto){
        return Question.builder()
            .id(questionDto.getId())
            .answers(answerDtoTransformer.transformDtoList(questionDto.getAnswers()))
            .text(questionDto.getText())
            .build();
    }

    public List<Question> transformDtoList(List<QuestionDto> questionDtos){
        return questionDtos.stream().map(this::transform).collect(Collectors.toList());
    }
}
