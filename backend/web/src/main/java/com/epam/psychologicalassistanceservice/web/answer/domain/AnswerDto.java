package com.epam.psychologicalassistanceservice.web.answer.domain;

import java.util.List;

import com.epam.psychologicalassistanceservice.web.emotioncorrelation.domain.EmotionCorrelationDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class AnswerDto {
    private Integer id;
    private String text;
    private List<EmotionCorrelationDto> emotionCorrelations;
}
