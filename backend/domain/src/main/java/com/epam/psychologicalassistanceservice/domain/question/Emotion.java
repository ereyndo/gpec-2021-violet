package com.epam.psychologicalassistanceservice.domain.question;

public enum Emotion {
    ANGER,
    FEAR,
    TRUST,
    JOY,
    SADNESS,
    SURPRISE,
    DISGUST,
    ANTICIPATION,
    HAPPINESS,
    CONFUSION,
    BOREDOM,
    ANXIETY
}
