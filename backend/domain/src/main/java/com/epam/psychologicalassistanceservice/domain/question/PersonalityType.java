package com.epam.psychologicalassistanceservice.domain.question;

public enum PersonalityType {
    HAPPY (1),
    ANGRY (2);

    private int representation;

    PersonalityType(int representation) {
        this.representation = representation;
    }
}
