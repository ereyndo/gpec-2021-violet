package com.epam.psychologicalassistanceservice.domain.question;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Questionnaire {
    private Integer id;
    private String title;
    private List<Question> questions;
    private String description;
}
