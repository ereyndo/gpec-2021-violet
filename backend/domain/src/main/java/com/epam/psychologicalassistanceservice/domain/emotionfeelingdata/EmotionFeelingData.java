package com.epam.psychologicalassistanceservice.domain.emotionfeelingdata;

import java.time.ZonedDateTime;
import java.util.List;

import com.epam.psychologicalassistanceservice.domain.question.Emotion;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EmotionFeelingData {
    private Integer id;
    private ZonedDateTime date;
    private List<Emotion> emotions;
    private String reasonOfFeeling;
    private String userId;
}
