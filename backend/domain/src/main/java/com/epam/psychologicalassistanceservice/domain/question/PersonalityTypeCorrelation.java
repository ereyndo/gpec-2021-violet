package com.epam.psychologicalassistanceservice.domain.question;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class PersonalityTypeCorrelation {
    private Integer id;
    private PersonalityType personalityType;
    private int points;
}
