package com.epam.psychologicalassistanceservice.domain.user;

import java.util.List;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class User {
    private String email;
    private String username;
    private String password;
    private List<EmotionFeelingData> emotionFeelingHistory;
}
