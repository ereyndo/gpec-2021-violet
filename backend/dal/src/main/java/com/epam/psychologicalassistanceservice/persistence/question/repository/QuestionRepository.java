package com.epam.psychologicalassistanceservice.persistence.question.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionEntity;

public interface QuestionRepository extends JpaRepository<QuestionEntity, Integer> {
}
