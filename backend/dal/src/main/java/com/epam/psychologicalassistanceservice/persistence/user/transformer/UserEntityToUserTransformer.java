package com.epam.psychologicalassistanceservice.persistence.user.transformer;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.persistence.user.domain.UserEntity;

@Component
public class UserEntityToUserTransformer {
    public User transform(UserEntity userEntity) {
        return User.builder()
            .email(userEntity.getEmail())
            .username(userEntity.getUsername())
            .password(userEntity.getPassword())
            .build();
    }
}
