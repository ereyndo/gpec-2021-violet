package com.epam.psychologicalassistanceservice.persistence.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.psychologicalassistanceservice.persistence.user.domain.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, String> {
}
