package com.epam.psychologicalassistanceservice.persistence.user.transformer;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.persistence.user.domain.UserEntity;

@Component
public class UserToUserEntityTransformer {
    public UserEntity transform(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(user.getEmail());
        userEntity.setUsername(user.getUsername());
        userEntity.setPassword(user.getPassword());
        return userEntity;
    }
}
