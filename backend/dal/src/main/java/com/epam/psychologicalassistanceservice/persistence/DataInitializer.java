package com.epam.psychologicalassistanceservice.persistence;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.repository.EmotionFeelingDataRepository;
import com.epam.psychologicalassistanceservice.persistence.question.domain.AnswerEntity;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionCorrelationEntity;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionEntity;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionEntity;
import com.epam.psychologicalassistanceservice.persistence.question.repository.QuestionRepository;
import com.epam.psychologicalassistanceservice.persistence.user.domain.UserEntity;
import com.epam.psychologicalassistanceservice.persistence.user.repository.UserRepository;

@Component
public class DataInitializer implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private EmotionFeelingDataRepository emotionFeelingDataRepository;

    @Override
    public void run(String... args){
        initUsers();
        initQuestions();
        initEmotionData();
    }

    private void initUsers() {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail("user");
        userEntity.setUsername("username");
        userEntity.setPassword("password");
        userRepository.save(userEntity);
    }

    private void initQuestions() {
        List<QuestionEntity> questionEntities = new ArrayList<>();

        // TEST QUESTION 1
        QuestionEntity questionEntity1 = new QuestionEntity();
        questionEntity1.setText("What quote or saying do people spout that you think is total BS?");

        AnswerEntity answerEntity1 = new AnswerEntity();
        answerEntity1.setText("\"Do one thing every day that scares you.\"");
        EmotionCorrelationEntity emotionCorrelationEntity1 = new EmotionCorrelationEntity();
        emotionCorrelationEntity1.setEmotionEntity(EmotionEntity.FEAR);
        emotionCorrelationEntity1.setPoints(1);
        answerEntity1.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity1));

        AnswerEntity answerEntity2 = new AnswerEntity();
        answerEntity2.setText("\"Don't cry because it's over, smile because it happened.\" — Dr. Seuss");
        EmotionCorrelationEntity emotionCorrelationEntity2 = new EmotionCorrelationEntity();
        emotionCorrelationEntity2.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity2.setPoints(1);
        answerEntity2.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity2));

        AnswerEntity answerEntity3 = new AnswerEntity();
        answerEntity3.setText("\"Life is short and then you die.\"");
        EmotionCorrelationEntity emotionCorrelationEntity3 = new EmotionCorrelationEntity();
        emotionCorrelationEntity3.setEmotionEntity(EmotionEntity.CONFUSION);
        emotionCorrelationEntity3.setPoints(1);
        answerEntity3.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity3));

        AnswerEntity answerEntity4 = new AnswerEntity();
        answerEntity4.setText("\"Fire in the heart sends smoke into the head.\" — German Proverb");
        EmotionCorrelationEntity emotionCorrelationEntity4 = new EmotionCorrelationEntity();
        emotionCorrelationEntity4.setEmotionEntity(EmotionEntity.CONFUSION);
        emotionCorrelationEntity4.setPoints(1);
        answerEntity4.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity4));

        questionEntity1.setAnswerEntities(List.of(answerEntity1, answerEntity2, answerEntity3, answerEntity4));
        questionEntities.add(questionEntity1);

        // TEST QUESTION 2
        QuestionEntity questionEntity2 = new QuestionEntity();
        questionEntity2.setText("On a scale of 1 to never, how often do you lose your temper?");

        AnswerEntity answerEntity5 = new AnswerEntity();
        answerEntity5.setText("You can call me a cucumber 'cause I always keep my cool");
        EmotionCorrelationEntity emotionCorrelationEntity5 = new EmotionCorrelationEntity();
        emotionCorrelationEntity5.setEmotionEntity(EmotionEntity.BOREDOM);
        emotionCorrelationEntity5.setPoints(1);
        answerEntity5.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity5));

        AnswerEntity answerEntity6 = new AnswerEntity();
        answerEntity6.setText("Zen is USUALLY the name of my game.");
        EmotionCorrelationEntity emotionCorrelationEntity6 = new EmotionCorrelationEntity();
        emotionCorrelationEntity6.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity6.setPoints(1);
        answerEntity6.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity6));

        AnswerEntity answerEntity7 = new AnswerEntity();
        answerEntity7.setText("I'm a hothead. It doesn't take much to push me over the edge...");
        EmotionCorrelationEntity emotionCorrelationEntity7 = new EmotionCorrelationEntity();
        emotionCorrelationEntity7.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity7.setPoints(1);
        answerEntity7.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity7));

        questionEntity2.setAnswerEntities(List.of(answerEntity5, answerEntity6, answerEntity7));
        questionEntities.add(questionEntity2);

        // TEST QUESTION 3
        QuestionEntity questionEntity3 = new QuestionEntity();
        questionEntity3.setText("You’re feeling bummed when your BFF backs out of your dinner plans, what happens next?...");

        AnswerEntity answerEntity8 = new AnswerEntity();
        answerEntity8.setText("I mope around for a bit then order some sushi take out and pour myself a generous glass of wine.");
        EmotionCorrelationEntity emotionCorrelationEntity8 = new EmotionCorrelationEntity();
        emotionCorrelationEntity8.setEmotionEntity(EmotionEntity.SADNESS);
        emotionCorrelationEntity8.setPoints(1);
        answerEntity8.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity8));

        AnswerEntity answerEntity9 = new AnswerEntity();
        answerEntity9.setText("My thoughts start spiralling — maybe they secretly think I'm annoying and are going out with someone cooler.");
        EmotionCorrelationEntity emotionCorrelationEntity9 = new EmotionCorrelationEntity();
        emotionCorrelationEntity9.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity9.setPoints(1);
        answerEntity9.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity9));

        AnswerEntity answerEntity10 = new AnswerEntity();
        answerEntity10.setText("No biggie! I pull out a cookbook and try my hand at a new gluten-free macaroni and cheese recipe.");
        EmotionCorrelationEntity emotionCorrelationEntity10 = new EmotionCorrelationEntity();
        emotionCorrelationEntity10.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity10.setPoints(1);
        answerEntity10.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity10));

        AnswerEntity answerEntity11 = new AnswerEntity();
        answerEntity11.setText("I obsess over how rude they were... Who cancels at the last minute?! Maybe it's time to write them off.");
        EmotionCorrelationEntity emotionCorrelationEntity11 = new EmotionCorrelationEntity();
        emotionCorrelationEntity11.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity11.setPoints(1);
        answerEntity11.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity11));

        questionEntity3.setAnswerEntities(List.of(answerEntity8, answerEntity9, answerEntity10, answerEntity11));
        questionEntities.add(questionEntity3);

        // TEST QUESTION 4
        QuestionEntity questionEntity4 = new QuestionEntity();
        questionEntity4.setText("How would your BFF describe you?");

        AnswerEntity answerEntity12 = new AnswerEntity();
        answerEntity12.setText("Creative, inspired, and a teensy bit emotional — I'm the friend that always starts crying during sad movies.");
        EmotionCorrelationEntity emotionCorrelationEntity12 = new EmotionCorrelationEntity();
        emotionCorrelationEntity12.setEmotionEntity(EmotionEntity.SADNESS);
        emotionCorrelationEntity12.setPoints(1);
        answerEntity12.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity12));

        AnswerEntity answerEntity13 = new AnswerEntity();
        answerEntity13.setText("Driven, determined, and never afraid to tell it like it is... Even if \"like it is\" might hurt a few feelings.");
        EmotionCorrelationEntity emotionCorrelationEntity13 = new EmotionCorrelationEntity();
        emotionCorrelationEntity13.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity13.setPoints(1);
        answerEntity13.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity13));

        AnswerEntity answerEntity14 = new AnswerEntity();
        answerEntity14.setText("Bubbly, light-hearted, and always has something nice to say no matter the circumstances. I like to spread the love.");
        EmotionCorrelationEntity emotionCorrelationEntity14 = new EmotionCorrelationEntity();
        emotionCorrelationEntity14.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity14.setPoints(1);
        answerEntity14.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity14));

        AnswerEntity answerEntity15 = new AnswerEntity();
        answerEntity15.setText("Quiet, sensitive, and always supportive. While I'm not a risk-taker, I encourage my friends to live life on the edge.");
        EmotionCorrelationEntity emotionCorrelationEntity15 = new EmotionCorrelationEntity();
        emotionCorrelationEntity15.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity15.setPoints(1);
        answerEntity15.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity15));

        questionEntity4.setAnswerEntities(List.of(answerEntity12, answerEntity13, answerEntity14, answerEntity15));
        questionEntities.add(questionEntity4);

        // TEST QUESTION 5
        QuestionEntity questionEntity5 = new QuestionEntity();
        questionEntity5.setText("The barista made your latte with almond milk, but you wanted oat milk, what do you do...");

        AnswerEntity answerEntity16 = new AnswerEntity();
        answerEntity16.setText("I'm too nervous to ask them to remake it, so I take my almond milk latte and exit stage left.");
        EmotionCorrelationEntity emotionCorrelationEntity16 = new EmotionCorrelationEntity();
        emotionCorrelationEntity16.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity16.setPoints(1);
        answerEntity16.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity16));

        AnswerEntity answerEntity17 = new AnswerEntity();
        answerEntity17.setText("They better watch out! I paid $7 for this latte, and if I ask for oat milk, I'm getting oat milk.");
        EmotionCorrelationEntity emotionCorrelationEntity17 = new EmotionCorrelationEntity();
        emotionCorrelationEntity17.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity17.setPoints(1);
        answerEntity17.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity17));

        AnswerEntity answerEntity18 = new AnswerEntity();
        answerEntity18.setText("I'm upset — it's like no one hears me anymore. I don't ask for a new one, but I feel down about it for an hour or so.");
        EmotionCorrelationEntity emotionCorrelationEntity18 = new EmotionCorrelationEntity();
        emotionCorrelationEntity18.setEmotionEntity(EmotionEntity.SADNESS);
        emotionCorrelationEntity18.setPoints(1);
        answerEntity18.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity18));

        AnswerEntity answerEntity19 = new AnswerEntity();
        answerEntity19.setText("No sweat! I wave at the barista and tell them that I ordered oat milk. I'm sure they'll remake it.");
        EmotionCorrelationEntity emotionCorrelationEntity19 = new EmotionCorrelationEntity();
        emotionCorrelationEntity19.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity19.setPoints(1);
        answerEntity19.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity19));

        questionEntity5.setAnswerEntities(List.of(answerEntity16, answerEntity17, answerEntity18, answerEntity19));
        questionEntities.add(questionEntity5);

        // TEST QUESTION 6
        QuestionEntity questionEntity6 = new QuestionEntity();
        questionEntity6.setText("What question do you ask yourself most?");

        AnswerEntity answerEntity20 = new AnswerEntity();
        answerEntity20.setText("What is the world, and why am I in it?");
        EmotionCorrelationEntity emotionCorrelationEntity20 = new EmotionCorrelationEntity();
        emotionCorrelationEntity20.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity20.setPoints(1);
        answerEntity20.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity20));

        AnswerEntity answerEntity21 = new AnswerEntity();
        answerEntity21.setText("Why do people walk so slow?");
        EmotionCorrelationEntity emotionCorrelationEntity21 = new EmotionCorrelationEntity();
        emotionCorrelationEntity21.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity21.setPoints(1);
        answerEntity21.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity21));

        AnswerEntity answerEntity22 = new AnswerEntity();
        answerEntity22.setText("Where are my social skills when I need them?");
        EmotionCorrelationEntity emotionCorrelationEntity22 = new EmotionCorrelationEntity();
        emotionCorrelationEntity22.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity22.setPoints(1);
        answerEntity22.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity22));

        AnswerEntity answerEntity23 = new AnswerEntity();
        answerEntity23.setText("What should I have for dinner?");
        EmotionCorrelationEntity emotionCorrelationEntity23 = new EmotionCorrelationEntity();
        emotionCorrelationEntity23.setEmotionEntity(EmotionEntity.BOREDOM);
        emotionCorrelationEntity23.setPoints(1);
        answerEntity23.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity23));

        questionEntity6.setAnswerEntities(List.of(answerEntity20, answerEntity21, answerEntity22, answerEntity23));
        questionEntities.add(questionEntity6);

        // TEST QUESTION 7
        QuestionEntity questionEntity7 = new QuestionEntity();
        questionEntity7.setText("Stop to smell the roses, yea or nay?");

        AnswerEntity answerEntity24 = new AnswerEntity();
        answerEntity24.setText("Roses, daises, and black-eyes Susans — I'm stopping to smell them all!");
        EmotionCorrelationEntity emotionCorrelationEntity24 = new EmotionCorrelationEntity();
        emotionCorrelationEntity24.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity24.setPoints(1);
        answerEntity24.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity24));

        AnswerEntity answerEntity25 = new AnswerEntity();
        answerEntity25.setText("Maybe... If I check them first for ants.");
        EmotionCorrelationEntity emotionCorrelationEntity25 = new EmotionCorrelationEntity();
        emotionCorrelationEntity25.setEmotionEntity(EmotionEntity.FEAR);
        emotionCorrelationEntity25.setPoints(1);
        answerEntity25.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity25));

        AnswerEntity answerEntity26 = new AnswerEntity();
        answerEntity26.setText("I ain't got time for flowers! I've got wars to wage and worlds to change.");
        EmotionCorrelationEntity emotionCorrelationEntity26 = new EmotionCorrelationEntity();
        emotionCorrelationEntity26.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity26.setPoints(1);
        answerEntity26.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity26));

        AnswerEntity answerEntity27 = new AnswerEntity();
        answerEntity27.setText("I smell them, cut them, and press them between the pages of my fave book!");
        EmotionCorrelationEntity emotionCorrelationEntity27 = new EmotionCorrelationEntity();
        emotionCorrelationEntity27.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity27.setPoints(1);
        answerEntity27.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity27));

        questionEntity7.setAnswerEntities(List.of(answerEntity24, answerEntity25, answerEntity26, answerEntity27));
        questionEntities.add(questionEntity7);

        // TEST QUESTION 8
        QuestionEntity questionEntity8 = new QuestionEntity();
        questionEntity8.setText("If you could master any of the following social-emotional skills, which would you choose and why?");

        AnswerEntity answerEntity28 = new AnswerEntity();
        answerEntity28.setText("Appreciating what I'm experiencing in the moment without worrying about it ending.");
        EmotionCorrelationEntity emotionCorrelationEntity28 = new EmotionCorrelationEntity();
        emotionCorrelationEntity28.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity28.setPoints(1);
        answerEntity28.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity28));

        AnswerEntity answerEntity29 = new AnswerEntity();
        answerEntity29.setText("Communicating my dislike for certain people, places, and things without coming off as aggressive.");
        EmotionCorrelationEntity emotionCorrelationEntity29 = new EmotionCorrelationEntity();
        emotionCorrelationEntity29.setEmotionEntity(EmotionEntity.ANGER);
        emotionCorrelationEntity29.setPoints(1);
        answerEntity29.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity29));

        AnswerEntity answerEntity30 = new AnswerEntity();
        answerEntity30.setText("Remembering the good even while I'm living through the bad. It always gets better, right?");
        EmotionCorrelationEntity emotionCorrelationEntity30 = new EmotionCorrelationEntity();
        emotionCorrelationEntity30.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity30.setPoints(1);
        answerEntity30.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity30));

        AnswerEntity answerEntity31 = new AnswerEntity();
        answerEntity31.setText("Accepting myself and not worrying so much about what others think of me.");
        EmotionCorrelationEntity emotionCorrelationEntity31 = new EmotionCorrelationEntity();
        emotionCorrelationEntity31.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity31.setPoints(1);
        answerEntity31.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity31));

        questionEntity8.setAnswerEntities(List.of(answerEntity28, answerEntity29, answerEntity30, answerEntity31));
        questionEntities.add(questionEntity8);

        // TEST QUESTION 9
        QuestionEntity questionEntity9 = new QuestionEntity();
        questionEntity9.setText("What's your BIG \"why\" behind balancing your emotions?");

        AnswerEntity answerEntity32 = new AnswerEntity();
        answerEntity32.setText("Strengthening my relationships with friends and family.");
        EmotionCorrelationEntity emotionCorrelationEntity32 = new EmotionCorrelationEntity();
        emotionCorrelationEntity32.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity32.setPoints(1);
        answerEntity32.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity32));

        AnswerEntity answerEntity33 = new AnswerEntity();
        answerEntity33.setText("Becoming more resilient, so I can handle the tough stuff.");
        EmotionCorrelationEntity emotionCorrelationEntity33 = new EmotionCorrelationEntity();
        emotionCorrelationEntity33.setEmotionEntity(EmotionEntity.SADNESS);
        emotionCorrelationEntity33.setPoints(1);
        answerEntity33.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity33));

        AnswerEntity answerEntity34 = new AnswerEntity();
        answerEntity34.setText("Getting my physical health in check. Stress hurts, people!");
        EmotionCorrelationEntity emotionCorrelationEntity34 = new EmotionCorrelationEntity();
        emotionCorrelationEntity34.setEmotionEntity(EmotionEntity.ANXIETY);
        emotionCorrelationEntity34.setPoints(1);
        answerEntity34.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity34));

        AnswerEntity answerEntity35 = new AnswerEntity();
        answerEntity35.setText("Cultivating my calm and carrying myself with confidence.");
        EmotionCorrelationEntity emotionCorrelationEntity35 = new EmotionCorrelationEntity();
        emotionCorrelationEntity35.setEmotionEntity(EmotionEntity.HAPPINESS);
        emotionCorrelationEntity35.setPoints(1);
        answerEntity35.setEmotionCorrelationEntities(List.of(emotionCorrelationEntity35));

        questionEntity9.setAnswerEntities(List.of(answerEntity32, answerEntity33, answerEntity34, answerEntity35));
        questionEntities.add(questionEntity9);

        questionRepository.saveAll(questionEntities);
    }

    private void initEmotionData() {
        EmotionFeelingDataEntity emotionFeelingDataEntity = new EmotionFeelingDataEntity();
        LocalDateTime timeStamp = LocalDateTime.of(2021, 1, 1, 1, 1, 1);
        emotionFeelingDataEntity.setDate(ZonedDateTime.of(timeStamp, ZoneId.systemDefault()));
        emotionFeelingDataEntity.setReasonOfFeeling("Nothing in particular...");
        emotionFeelingDataEntity.setUserId("user");
        emotionFeelingDataEntity.setEmotionEntities(List.of(EmotionEntity.ANGER, EmotionEntity.JOY));
        emotionFeelingDataRepository.save(emotionFeelingDataEntity);
    }
}
