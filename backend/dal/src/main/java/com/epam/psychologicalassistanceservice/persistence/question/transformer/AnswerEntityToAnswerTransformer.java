package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Answer;
import com.epam.psychologicalassistanceservice.persistence.question.domain.AnswerEntity;

@Component
public class AnswerEntityToAnswerTransformer {
    @Autowired
    EmotionCorrelationEntityToEmotionCorrelationTransformer emotionCorrelationTransformer;

    public Answer transform(AnswerEntity answerEntity) {
        return Answer.builder()
            .id(answerEntity.getId())
            .text(answerEntity.getText())
            .emotionCorrelations(emotionCorrelationTransformer.transformMultiple(answerEntity.getEmotionCorrelationEntities()))
            .build();
    }

    public List<Answer> transformMultiple(List<AnswerEntity> answerEntities) {
        return answerEntities.stream().map(this::transform).collect(Collectors.toList());
    }
}
