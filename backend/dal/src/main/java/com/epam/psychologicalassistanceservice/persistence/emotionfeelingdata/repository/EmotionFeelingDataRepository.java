package com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;

public interface EmotionFeelingDataRepository extends JpaRepository<EmotionFeelingDataEntity, Integer> {

    List<EmotionFeelingDataEntity> findAllByUserId(String userId);
}
