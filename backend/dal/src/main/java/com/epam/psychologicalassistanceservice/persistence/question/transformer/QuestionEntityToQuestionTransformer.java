package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionEntity;

@Component
public class QuestionEntityToQuestionTransformer {
    @Autowired
    private AnswerEntityToAnswerTransformer toAnswerEntityTransformer;

    public Question transform(QuestionEntity questionEntity) {
        return Question.builder()
            .id(questionEntity.getId())
            .answers(toAnswerEntityTransformer.transformMultiple(questionEntity.getAnswerEntities()))
            .text(questionEntity.getText())
            .build();
    }

    public List<Question> transformMultiple(List<QuestionEntity> questionEntities) {
        return questionEntities.stream().map(this::transform).collect(Collectors.toList());
    }
}
