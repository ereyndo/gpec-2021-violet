package com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Table;

import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
@Entity
@Table(name = "emotion_feeling_data")
public class EmotionFeelingDataEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    Integer id;

    ZonedDateTime date;

    @Enumerated(EnumType.STRING)
    @Column(name = "emotion", nullable = false)
    @ElementCollection(targetClass = EmotionEntity.class)
    @JoinTable(name = "emotion_feeling_data_emotion", joinColumns = @JoinColumn(name = "emotion_feeling_data_id"))
    private List<EmotionEntity> emotionEntities;

    private String reasonOfFeeling;

    @Column(name = "user_id")
    private String userId;
}
