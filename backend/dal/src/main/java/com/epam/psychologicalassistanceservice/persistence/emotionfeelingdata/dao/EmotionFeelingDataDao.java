package com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.repository.EmotionFeelingDataRepository;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.transformer.EmotionFeelingDataEntityToEmotionFeelingDataTransformer;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.transformer.EmotionFeelingDataToEmotionFeelingDataEntityTransformer;

@Component
public class EmotionFeelingDataDao {
    @Autowired
    private EmotionFeelingDataRepository emotionFeelingDataRepository;

    @Autowired
    private EmotionFeelingDataToEmotionFeelingDataEntityTransformer toEmotionFeelingDataEntityTransformer;

    @Autowired
    private EmotionFeelingDataEntityToEmotionFeelingDataTransformer toEmotionFeelingDataTransformer;

    public EmotionFeelingData save(EmotionFeelingData emotionFeelingData) {
        EmotionFeelingDataEntity emotionFeelingDataEntity = toEmotionFeelingDataEntityTransformer.transform(emotionFeelingData);
        return toEmotionFeelingDataTransformer.transform(emotionFeelingDataRepository.save(emotionFeelingDataEntity));
    }

    public List<EmotionFeelingData> getAllByUserId(String userId) {
        return toEmotionFeelingDataTransformer.transformMultiple(emotionFeelingDataRepository.findAllByUserId(userId));
    }
}
