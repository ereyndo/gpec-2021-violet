package com.epam.psychologicalassistanceservice.persistence.question.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionEntity;
import com.epam.psychologicalassistanceservice.persistence.question.repository.QuestionRepository;
import com.epam.psychologicalassistanceservice.persistence.question.transformer.QuestionEntityToQuestionTransformer;
import com.epam.psychologicalassistanceservice.persistence.question.transformer.QuestionToQuestionEntityTransformer;

@Component
public class QuestionDao {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private QuestionToQuestionEntityTransformer toQuestionEntityTransformer;

    @Autowired
    private QuestionEntityToQuestionTransformer toQuestionTransformer;

    public Question save(Question question) {
        QuestionEntity questionEntity = toQuestionEntityTransformer.transform(question);
        return toQuestionTransformer.transform(questionRepository.save(questionEntity));
    }

    public Question findById(Integer id) {
        Optional<QuestionEntity> questionEntity = questionRepository.findById(id);
        if(questionEntity.isEmpty()){
            throw new RuntimeException("com.epam.psychologicalassistanceservice.domain.question.Question with this id does not exist!");
        } else {
            return toQuestionTransformer.transform(questionEntity.get());
        }
    }

    public List<Question> findAll() {
        return toQuestionTransformer.transformMultiple(questionRepository.findAll());
    }
}
