package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Emotion;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionEntity;

@Component
public class EmotionEntityToEmotionTransformer {
    public Emotion transform(EmotionEntity emotion) {
        return Emotion.valueOf(emotion.name());
    }

    public List<Emotion> transformMultiple(List<EmotionEntity> emotionEntities) {
        return emotionEntities.stream().map(this::transform).collect(Collectors.toList());
    }
}
