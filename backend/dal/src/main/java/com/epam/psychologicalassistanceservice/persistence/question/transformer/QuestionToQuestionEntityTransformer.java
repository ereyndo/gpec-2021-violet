package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionEntity;

@Component
public class QuestionToQuestionEntityTransformer {
    @Autowired
    private AnswerToAnswerEntityTransformer answerEntityTransformer;

    public QuestionEntity transform(Question question) {
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setId(question.getId());
        questionEntity.setAnswerEntities(answerEntityTransformer.transformMultiple(question.getAnswers()));
        questionEntity.setText(question.getText());
        return questionEntity;
    }

    public List<QuestionEntity> transformMultiple(List<Question> questions) {
        return questions.stream().map(this::transform).collect(Collectors.toList());
    }
}
