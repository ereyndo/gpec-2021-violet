package com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;
import com.epam.psychologicalassistanceservice.persistence.question.transformer.EmotionToEmotionEntityTransformer;

@Component
public class EmotionFeelingDataToEmotionFeelingDataEntityTransformer {
    @Autowired
    private EmotionToEmotionEntityTransformer emotionTransformer;

    public EmotionFeelingDataEntity transform(EmotionFeelingData emotionFeelingData) {
        EmotionFeelingDataEntity emotionFeelingDataEntity = new EmotionFeelingDataEntity();
        emotionFeelingDataEntity.setId(emotionFeelingData.getId());
        emotionFeelingDataEntity.setDate(emotionFeelingData.getDate());
        emotionFeelingDataEntity.setEmotionEntities(emotionTransformer.transformMultiple(emotionFeelingData.getEmotions()));
        emotionFeelingDataEntity.setReasonOfFeeling(emotionFeelingData.getReasonOfFeeling());
        emotionFeelingDataEntity.setUserId(emotionFeelingData.getUserId());
        return emotionFeelingDataEntity;
    }
}
