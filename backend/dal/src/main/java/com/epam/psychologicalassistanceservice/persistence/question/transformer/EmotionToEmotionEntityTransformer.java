package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Emotion;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionEntity;

@Component
public class EmotionToEmotionEntityTransformer {
    public EmotionEntity transform(Emotion emotion) {
        return EmotionEntity.valueOf(emotion.name());
    }

    public List<EmotionEntity> transformMultiple(List<Emotion> emotions) {
        return emotions.stream().map(this::transform).collect(Collectors.toList());
    }
}
