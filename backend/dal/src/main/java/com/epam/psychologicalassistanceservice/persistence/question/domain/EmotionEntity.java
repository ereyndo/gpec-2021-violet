package com.epam.psychologicalassistanceservice.persistence.question.domain;

public enum EmotionEntity {
    ANGER,
    FEAR,
    TRUST,
    JOY,
    SADNESS,
    SURPRISE,
    DISGUST,
    ANTICIPATION,
    HAPPINESS,
    CONFUSION,
    BOREDOM,
    ANXIETY
}
