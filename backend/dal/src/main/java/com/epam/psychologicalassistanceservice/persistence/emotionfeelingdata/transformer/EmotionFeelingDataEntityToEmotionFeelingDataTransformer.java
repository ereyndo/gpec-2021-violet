package com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;
import com.epam.psychologicalassistanceservice.persistence.question.transformer.EmotionEntityToEmotionTransformer;

@Component
public class EmotionFeelingDataEntityToEmotionFeelingDataTransformer {
    @Autowired
    private EmotionEntityToEmotionTransformer emotionTransformer;

    public EmotionFeelingData transform(EmotionFeelingDataEntity emotionFeelingDataEntity) {
        return EmotionFeelingData.builder()
            .id(emotionFeelingDataEntity.getId())
            .date(emotionFeelingDataEntity.getDate())
            .emotions(emotionTransformer.transformMultiple(emotionFeelingDataEntity.getEmotionEntities()))
            .reasonOfFeeling(emotionFeelingDataEntity.getReasonOfFeeling())
            .userId(emotionFeelingDataEntity.getUserId())
            .build();
    }

    public List<EmotionFeelingData> transformMultiple(List<EmotionFeelingDataEntity> emotionFeelingDataList) {
        return emotionFeelingDataList.stream().map(this::transform).collect(Collectors.toList());
    }
}
