package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.EmotionCorrelation;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionCorrelationEntity;

@Component
public class EmotionCorrelationEntityToEmotionCorrelationTransformer {
    @Autowired
    EmotionEntityToEmotionTransformer emotionTransformer;

    public EmotionCorrelation transform(EmotionCorrelationEntity emotionCorrelationEntity) {
        return EmotionCorrelation.builder()
            .id(emotionCorrelationEntity.getId())
            .points(emotionCorrelationEntity.getPoints())
            .emotion(emotionTransformer.transform(emotionCorrelationEntity.getEmotionEntity()))
            .build();
    }

    public List<EmotionCorrelation> transformMultiple(List<EmotionCorrelationEntity> emotionCorrelationEntities) {
        return emotionCorrelationEntities.stream().map(this::transform).collect(Collectors.toList());
    }

}
