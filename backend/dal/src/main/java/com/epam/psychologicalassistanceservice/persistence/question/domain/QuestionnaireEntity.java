package com.epam.psychologicalassistanceservice.persistence.question.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

//@Entity
@Getter
@Setter
@Table(name = "questionnaire")
public class QuestionnaireEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String title;
    @OneToMany(cascade= CascadeType.ALL)
    @JoinColumn(name="questionnaire_id")
    private List<QuestionEntity> questionEntities;
    private String description;
}
