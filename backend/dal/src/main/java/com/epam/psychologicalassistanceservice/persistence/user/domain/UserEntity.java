package com.epam.psychologicalassistanceservice.persistence.user.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.domain.EmotionFeelingDataEntity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "user")
public class UserEntity {

    @Id
    private String email;
    private String username;
    private String password;
    @OneToMany(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="user_id")
    private List<EmotionFeelingDataEntity> emotionFeelingHistory;
}
