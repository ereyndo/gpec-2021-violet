package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Questionnaire;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionnaireEntity;

@Component
public class QuestionnaireToQuestionnaireEntityTransformer {
    @Autowired
    private QuestionToQuestionEntityTransformer toQuestionTransformer;

    public QuestionnaireEntity transform(Questionnaire questionnaire) {
        QuestionnaireEntity questionnaireEntity = new QuestionnaireEntity();
        questionnaireEntity.setId(questionnaire.getId());
        questionnaireEntity.setTitle(questionnaire.getTitle());
        questionnaireEntity.setDescription(questionnaire.getDescription());
        questionnaireEntity.setQuestionEntities(toQuestionTransformer.transformMultiple(questionnaire.getQuestions()));
        return questionnaireEntity;
    }
}
