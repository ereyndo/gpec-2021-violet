package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.EmotionCorrelation;
import com.epam.psychologicalassistanceservice.persistence.question.domain.EmotionCorrelationEntity;

@Component
public class EmotionCorrelationToEmotionCorrelationEntityTransformer {
    @Autowired
    EmotionToEmotionEntityTransformer emotionTransformer;

    public EmotionCorrelationEntity transform(EmotionCorrelation emotionCorrelation) {
        EmotionCorrelationEntity emotionCorrelationEntity = new EmotionCorrelationEntity();
        emotionCorrelationEntity.setId(emotionCorrelation.getId());
        emotionCorrelationEntity.setPoints(emotionCorrelation.getPoints());
        emotionCorrelationEntity.setEmotionEntity(emotionTransformer.transform(emotionCorrelation.getEmotion()));
        return emotionCorrelationEntity;
    }

    public List<EmotionCorrelationEntity> transformMultiple(List<EmotionCorrelation> emotionCorrelations) {
        return emotionCorrelations.stream().map(this::transform).collect(Collectors.toList());
    }

}
