package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Questionnaire;
import com.epam.psychologicalassistanceservice.persistence.question.domain.QuestionnaireEntity;

@Component
public class QuestionnaireEntityToQuestionnaireTransformer {
    @Autowired
    private QuestionEntityToQuestionTransformer toQuestionTransformer;

    public Questionnaire transform(QuestionnaireEntity questionnaireEntity) {
        return Questionnaire.builder()
            .id(questionnaireEntity.getId())
            .title(questionnaireEntity.getTitle())
            .description(questionnaireEntity.getDescription())
            .questions(toQuestionTransformer.transformMultiple(questionnaireEntity.getQuestionEntities()))
            .build();
    }
}
