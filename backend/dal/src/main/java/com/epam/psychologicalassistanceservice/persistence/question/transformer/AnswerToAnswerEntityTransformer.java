package com.epam.psychologicalassistanceservice.persistence.question.transformer;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.question.Answer;
import com.epam.psychologicalassistanceservice.persistence.question.domain.AnswerEntity;

@Component
public class AnswerToAnswerEntityTransformer {
    @Autowired
    private EmotionCorrelationToEmotionCorrelationEntityTransformer emotionCorrelationTransformer;

    public AnswerEntity transform(Answer answer) {
        AnswerEntity answerEntity = new AnswerEntity();
        answerEntity.setId(answer.getId());
        answerEntity.setText(answer.getText());
        answerEntity.setEmotionCorrelationEntities(emotionCorrelationTransformer.transformMultiple(answer.getEmotionCorrelations()));
        return answerEntity;
    }

    public List<AnswerEntity> transformMultiple(List<Answer> answers) {
        return answers.stream().map(this::transform).collect(Collectors.toList());
    }
}
