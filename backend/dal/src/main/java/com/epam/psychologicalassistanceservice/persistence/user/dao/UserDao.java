package com.epam.psychologicalassistanceservice.persistence.user.dao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.persistence.user.domain.UserEntity;
import com.epam.psychologicalassistanceservice.persistence.user.repository.UserRepository;
import com.epam.psychologicalassistanceservice.persistence.user.transformer.UserEntityToUserTransformer;
import com.epam.psychologicalassistanceservice.persistence.user.transformer.UserToUserEntityTransformer;

@Component
public class UserDao {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserEntityToUserTransformer toUserTransformer;

    @Autowired
    private UserToUserEntityTransformer toUserEntityTransformer;

    public User findByEmail(String email) {
        Optional<UserEntity> userEntity = userRepository.findById(email);
        if(userEntity.isEmpty()){
            throw new RuntimeException("com.epam.psychologicalassistanceservice.domain.user.User with this id does not exist!");
        } else {
            return toUserTransformer.transform(userEntity.get());
        }
    }

    public User save(User user) {
        if (userRepository.existsById(user.getEmail())) {
            throw new RuntimeException("com.epam.psychologicalassistanceservice.domain.user.User already exists with this id!");
        }
        UserEntity userEntity = toUserEntityTransformer.transform(user);
        return toUserTransformer.transform(userRepository.save(userEntity));
    }

    public User modify(User user) {
        if (!userRepository.existsById(user.getEmail())) {
            throw new RuntimeException("com.epam.psychologicalassistanceservice.domain.user.User with this id does not exist!");
        }
        UserEntity userEntity = toUserEntityTransformer.transform(user);
        return toUserTransformer.transform(userRepository.save(userEntity));
    }
}
