# Requirements to run
## java 11

# How to run
cd to this directory

./mvnw clean install

./mvnw spring-boot:run

# How to reach the backend
http://localhost:8080

# How to reach the database
http://localhost:8080/h2-console

Driver Class: org.h2.Driver

JDBC URL: jdbc:h2:mem:testdb

User Name: sa

Password: password

# Additional information
## The database is only available while the backend running, and the changes made between two restarts will not persists.
