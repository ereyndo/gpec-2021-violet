package com.epam.psychologicalassistanceservice.service.question;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.psychologicalassistanceservice.domain.question.Question;
import com.epam.psychologicalassistanceservice.persistence.question.dao.QuestionDao;

@Service
public class QuestionService {
    @Autowired
    private QuestionDao questionDao;

    public Question save(Question question) {
        return questionDao.save(question);
    }

    public Question getById(Integer questionId) {
        return questionDao.findById(questionId);
    }

    public List<Question> getAll() {
        return questionDao.findAll();
    }
}
