package com.epam.psychologicalassistanceservice.service.emotionfeelingdata;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.epam.psychologicalassistanceservice.domain.emotionfeelingdata.EmotionFeelingData;
import com.epam.psychologicalassistanceservice.persistence.emotionfeelingdata.dao.EmotionFeelingDataDao;

@Service
public class EmotionFeelingService {
    @Autowired
    private EmotionFeelingDataDao emotionFeelingDataDao;

    public EmotionFeelingData saveOfCurrentUser(EmotionFeelingData emotionFeelingData) {
        emotionFeelingData.setUserId(getUserId());
        emotionFeelingData.setDate(ZonedDateTime.now());
        return emotionFeelingDataDao.save(emotionFeelingData);
    }

    public List<EmotionFeelingData> findAllByUserId(String userId) {
        return emotionFeelingDataDao.getAllByUserId(userId);
    }

    public List<EmotionFeelingData> findAllOfCurrentUser() {
        return findAllByUserId(getUserId());
    }

    private String getUserId() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
    }
}
