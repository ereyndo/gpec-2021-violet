package com.epam.psychologicalassistanceservice.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.persistence.user.dao.UserDao;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public User save(User user) {
        return userDao.save(user);
    }

    public User findCurrentUser() {
        String email = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        return userDao.findByEmail(email);
    }

    public User modify(User user) {
        return userDao.modify(user);
    }
}
