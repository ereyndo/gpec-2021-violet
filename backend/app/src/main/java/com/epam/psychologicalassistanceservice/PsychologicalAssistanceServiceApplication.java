package com.epam.psychologicalassistanceservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PsychologicalAssistanceServiceApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context =
			SpringApplication.run(PsychologicalAssistanceServiceApplication.class, args);
	}
}
