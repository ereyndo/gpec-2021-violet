package com.epam.psychologicalassistanceservice.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.epam.psychologicalassistanceservice.domain.user.User;
import com.epam.psychologicalassistanceservice.persistence.user.dao.UserDao;

@Component
public class PsychologicalAssistanceServiceAuthenticationProvider implements AuthenticationProvider {
    private static final String ROLE_USER = "ROLE_USER";
    private static final String AUTHENTICATION_EXCEPTION = "External system authentication failed";

    @Autowired
    private UserDao userDao;

    @Override
    public Authentication authenticate(Authentication authentication)
        throws AuthenticationException {
        String email = authentication.getName();
        String password = authentication.getCredentials().toString();
        User user = userDao.findByEmail(email);
        if (authenticateUser(password, user)) {
            List<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(ROLE_USER));

            return new UsernamePasswordAuthenticationToken(
                email,
                password,
                authorities);
        } else {
            throw new BadCredentialsException(AUTHENTICATION_EXCEPTION);
        }
    }

    private boolean authenticateUser(String authenticationPassword, User user) {
        return user != null
            && user.getPassword().equals(authenticationPassword);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
