package com.example.violet_team.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import com.example.violet_team.R
import com.example.violet_team.data.LoginRepository
import com.example.violet_team.data.datasources.LoginDataSourceImpl
import com.example.violet_team.databinding.ActivityMainBinding
import com.example.violet_team.ui.ViewModelsFactory
import com.example.violet_team.ui.login.LoggedInUserView.Companion.DISPLAY_NAME
import com.example.violet_team.ui.login.LoggedInUserView.Companion.IMG_SRC
import com.example.violet_team.ui.login.LoginActivity
import com.example.violet_team.ui.test.TestActivity
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener{
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        val pref = getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
        if (!pref.getBoolean(getString(R.string.is_authenticated), false)) {
            toLogin()
        }
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.navView.setNavigationItemSelectedListener(this)

        val name =  pref.getString(DISPLAY_NAME, "User")
        //todo retrieve img by link
        val imgSrc = pref.getString(IMG_SRC, "")
        val header = binding.navView.getHeaderView(0)
        header.findViewById<TextView>(R.id.email).text = name
        header.findViewById<ImageView>(R.id.avatar)
            .setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.profile))
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_test ->{
                startActivity(Intent(this, TestActivity::class.java))
            }
            R.id.nav_questionnaires ->{
            }
            R.id.nav_feel_bad_guide -> {
            }
            R.id.nav_emotion_base -> {
            }
            R.id.nav_apointment_to_psy -> {
            }
            R.id.nav_logout -> {
                LoginRepository(LoginDataSourceImpl(ViewModelsFactory.dataSource)).logout()
                val prefs =
                    getSharedPreferences(getString(R.string.shared_pref), Context.MODE_PRIVATE)
                with(prefs.edit()) {
                    putBoolean(getString(R.string.is_authenticated), false)
                    apply()
                }
                toLogin()
            }
        }
        binding.drawerLayout.closeDrawer(Gravity.LEFT)
        return true
    }

    private fun toLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}