package com.example.violet_team.data

import com.example.violet_team.data.datasources.QuestionnaireDataSource
import com.example.violet_team.data.model.Emotion

class QuestionnaireRepository(val dataSource: QuestionnaireDataSource) {

    suspend fun getQuestions() = dataSource.getQuestions()

    fun getMoodFeelingHistory() = dataSource.getMoodFeelingHistory()

    fun saveMoodFeelingTest(emotions : List<Emotion>) = dataSource.postMoodFeelingsTest(emotions)
}