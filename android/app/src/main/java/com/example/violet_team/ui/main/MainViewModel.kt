package com.example.violet_team.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.violet_team.data.QuestionnaireRepository
import com.example.violet_team.data.model.Emotion

class MainViewModel(private val repository: QuestionnaireRepository) : ViewModel() {
    val currentEmotions = MutableLiveData(listOf<Emotion>())

    private val _emotionHistory =
        MutableLiveData(repository.getMoodFeelingHistory())
    val emotionHistory: LiveData<Map<Emotion, Int>> = _emotionHistory

    fun saveMoodFeelingsTest(text : String) {
        currentEmotions.value?.let {
            repository.saveMoodFeelingTest(it)
            val newMap = _emotionHistory.value?.toMutableMap() ?: mutableMapOf()
            it.forEach { emotion ->
                val key = newMap[emotion] ?: 0
                newMap[emotion] = key + 1
            }
            _emotionHistory.value = newMap
        }
        //todo save text
    }

    fun addCurrentEmotion(emotion: Emotion) {
        val list = currentEmotions.value?.toMutableList().also { it?.add(emotion) }
        currentEmotions.value = list
    }

    fun removeCurrentEmotion(emotion: Emotion) {
        currentEmotions.value = currentEmotions.value?.toMutableList().also { it?.remove(emotion) }
    }

}