package com.example.violet_team.data.model

data class Questionnaire(
    val id: Int,
    val title: String,
    val questions: List<Question>,
    val description: String
)
