package com.example.violet_team.data.datasources

import com.example.violet_team.data.model.Answer
import com.example.violet_team.data.model.Emotion
import com.example.violet_team.data.model.Question

class FakeQuestionnaireDataSource : QuestionnaireDataSource, LoginDataSource {
    override suspend fun getQuestions(): List<Question> = listOf(
        Question(
            1, listOf(
                Answer(1, Emotion.FEAR, "Do one thing every day that scares you"),
                Answer(2, Emotion.JOY, "Don't cry because it's over, smile because it happened"),
                Answer(3, Emotion.SADNESS, "Life is short and then you die"),
                Answer(4, Emotion.ANGER, "Fire in the heart sends smoke into the head")
            ),
            "What quote or saying do people spout that you think is total BS?"
        ),
        Question(
            2, listOf(
                Answer(3, Emotion.FEAR, "1-4 — You can call me a cucumber 'cause I always keep my cool \uD83E\uDD52"),
                Answer(4, Emotion.JOY, "5-7 — Zen is USUALLY the name of my game."),
                Answer(5, Emotion.ANGER, "8-10 — I'm a hothead. It doesn't take much to push me over the edge...")
            ),
            "On a scale of 1 to never, how often do you lose your temper?"
        ),
        Question(
            3, listOf(
                Answer(1, Emotion.FEAR, "I mope around for a bit then order some sushi take out and pour myself a generous glass of wine."),
                Answer(2, Emotion.SADNESS, "My thoughts start spiralling — maybe they secretly think I'm annoying and are going out with someone cooler."),
                Answer(3, Emotion.JOY, "No biggie! I pull out a cookbook and try my hand at a new gluten-free macaroni and cheese recipe."),
                Answer(4, Emotion.ANGER, "I obsess over how rude they were... Who cancels at the last minute?! Maybe it's time to write them off.")
            ),
            "You’re feeling bummed when your BFF backs out of your dinner plans, what happens next?"
        ),
        Question(
            4, listOf(
                Answer(1, Emotion.SADNESS, "Creative, inspired, and a teensy bit emotional — I'm the friend that always starts crying during sad movies."),
                Answer(2, Emotion.ANGER, "Driven, determined, and never afraid to tell it like it is... Even if \"like it is\" might hurt a few feelings."),
                Answer(3, Emotion.JOY, "Bubbly, light-hearted, and always has something nice to say no matter the circumstances. I like to spread the love ❤️"),
                Answer(4, Emotion.FEAR, "Quiet, sensitive, and always supportive. While I'm not a risk-taker, I encourage my friends to live life on the edge.")
            ),
            "How would your BFF describe you?"
        ),
        Question(
            5, listOf(
                Answer(1, Emotion.FEAR, "I'm too nervous to ask them to remake it, so I take my almond milk latte and exit stage left."),
                Answer(2, Emotion.ANGER, "They better watch out! I paid \$7 for this latte, and if I ask for oat milk, I'm getting oat milk."),
                Answer(3, Emotion.SADNESS, "I'm upset — it's like no one hears me anymore. I don't ask for a new one, but I feel down about it for an hour or so."),
                Answer(4, Emotion.JOY, "No sweat! I wave at the barista and tell them that I ordered oat milk. I'm sure they'll remake it.")
            ),
            "The barista made your latte with almond milk, but you wanted oat milk, what do you do..."
        ),
        Question(
            6, listOf(
                Answer(1, Emotion.SADNESS, "What is the world, and why am I in it?"),
                Answer(2, Emotion.ANGER, "Why do people walk so slow?"),
                Answer(3, Emotion.FEAR, "Where are my social skills when I need them?"),
                Answer(4, Emotion.JOY, "What should I have for dinner?")
            ),
            "What question do you ask yourself most?"
        ),
        Question(
            7, listOf(
                Answer(1, Emotion.SADNESS, "Roses, daises, and black-eyes Susans — I'm stopping to smell them all!"),
                Answer(2, Emotion.FEAR, "Maybe... If I check them first for ants."),
                Answer(3, Emotion.JOY, "I ain't got time for flowers! I've got wars to wage and worlds to change."),
                Answer(4, Emotion.ANGER, "I smell them, cut them, and press them between the pages of my fave book!")
            ),
            "Stop to smell the roses, yea or nay?"
        ),
        Question(
            8, listOf(
                Answer(1, Emotion.JOY, "Appreciating what I'm experiencing in the moment without worrying about it ending."),
                Answer(2, Emotion.ANGER, "Communicating my dislike for certain people, places, and things without coming off as aggressive."),
                Answer(3, Emotion.FEAR, "Remembering the good even while I'm living through the bad. It always gets better, right?"),
                Answer(4, Emotion.SADNESS, "Accepting myself and not worrying so much about what others think of me.")
            ),
            "If you could master any of the following social-emotional skills, which would you choose and why?"
        ),
        Question(
            9, listOf(
                Answer(1, Emotion.FEAR, "Strengthening my relationships with friends and family."),
                Answer(2, Emotion.SADNESS, "Becoming more resilient, so I can handle the tough stuff."),
                Answer(3, Emotion.JOY, "Getting my physical health in check. Stress hurts, people!"),
                Answer(4, Emotion.ANGER, "Cultivating my calm and carrying myself with confidence.")
            ),
            "What's your BIG \"why\" behind balancing your emotions?"
        )
    )

    override fun postMoodFeelingsTest(emotions: List<Emotion>) {
        emotions.forEach {
            val last = emotionHistory[it] ?: 0
            emotionHistory[it] = last
        }
    }

    override fun getMoodFeelingHistory(): Map<Emotion, Int> = emotionHistory


    override suspend fun login(email: String, password: String) {
    }

    override suspend fun register(email: String, password: String) {
    }

    private val emotionHistory = mutableMapOf<Emotion, Int>()
}
