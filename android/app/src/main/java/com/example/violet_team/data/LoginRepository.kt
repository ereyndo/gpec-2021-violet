package com.example.violet_team.data

import android.util.Log
import com.example.violet_team.data.datasources.LoginDataSourceImpl
import com.example.violet_team.data.model.User

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(val dataSource: LoginDataSourceImpl) {

    var user: User? = null
        private set

    val isLoggedIn: Boolean
        get() = user != null

    init {
        user = null
    }

    fun logout() {
        user = null
        dataSource.logout()
    }

    suspend fun login(email: String, password: String): Result<User> {
        val result = dataSource.login(email, password)
        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }else{
            Log.e(NET_TAG, "login: ${(result as Result.Error).exception.message}")
        }
        return result
    }
    suspend fun register(email: String, password: String) : Result<User>{
        val result = dataSource.register(email, password)
        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }else{
            Log.e(NET_TAG, "login: ${(result as Result.Error).exception.message}")
        }
        return result
    }
    private fun setLoggedInUser(user: User) {
        this.user = user
    }
}

const val NET_TAG = "Network"