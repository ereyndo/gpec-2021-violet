package com.example.violet_team.data.datasources

interface LoginDataSource {
    suspend fun login(email: String, password: String)
    suspend fun register(email: String, password: String)
}