package com.example.violet_team.data.model

data class PersonalityTypeCorrelation(val id:Int, val emotion:Emotion, val points :Int)
