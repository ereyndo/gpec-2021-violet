package com.example.violet_team.data.model

enum class Emoji(val emoji: String) {
    ANGER("😠"),
    FEAR("😰"),
    TRUST("😕"),
    JOY("😄"),
    SADNESS("😞"),
    SURPRISE("😮"),
    DISGUST("🤢"),
    ANTICIPATION("😒")
}