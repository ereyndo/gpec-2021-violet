package com.example.violet_team.ui.test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.violet_team.data.QuestionnaireRepository
import com.example.violet_team.data.model.Emotion
import com.example.violet_team.data.model.Question
import com.example.violet_team.data.model.Questionnaire
import kotlinx.coroutines.launch

class TestViewModel(private val repository: QuestionnaireRepository) : ViewModel(){
    private lateinit var questions : List<Question>
    init {
        viewModelScope.launch {
            questions = repository.getQuestions()
        }
    }
    var index = 0
    private val points = hashMapOf<Emotion, Int>()

    fun getPointsAmount(emotion: Emotion) = questions.flatMap { it.answers }
        .filter { it.emotion == emotion }
        .count()


    fun addPoint(emotion: Emotion) {
        val last = points[emotion] ?: 0
        points[emotion] = last + 1
    }
    fun getNextQuestion() = if (index < questions.size) questions[index++] else null

    fun getPoints() = points
}