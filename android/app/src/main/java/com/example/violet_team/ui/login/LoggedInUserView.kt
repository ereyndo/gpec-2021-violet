package com.example.violet_team.ui.login

/**
 * User details post authentication that is exposed to the UI
 */
data class LoggedInUserView(
    val displayName: String,
    val imageSrc : String
){
    companion object{
        const val DISPLAY_NAME = "display_name"
        const val IMG_SRC = "img_src"
    }
}