package com.example.violet_team.data.model

class PostResponse(val protocol: String, val code : Int, val message: String, val url : String)