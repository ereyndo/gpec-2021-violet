package com.example.violet_team.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.violet_team.data.LoginRepository
import com.example.violet_team.data.NET_TAG
import com.example.violet_team.data.QuestionnaireRepository
import com.example.violet_team.data.datasources.FakeQuestionnaireDataSource
import com.example.violet_team.data.datasources.LoginDataSourceImpl
import com.example.violet_team.retrofit.Dao
import com.example.violet_team.ui.login.LoginViewModel
import com.example.violet_team.ui.main.MainViewModel
import com.example.violet_team.ui.test.TestViewModel
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * ViewModel provider factory to instantiate LoginViewModel.
 * Required given LoginViewModel has a non-empty constructor
 */
object ViewModelsFactory : ViewModelProvider.Factory {
    val retrofit: Dao
    val dataSource = FakeQuestionnaireDataSource()

    init {
        val client = OkHttpClient.Builder().addInterceptor {
            val r = it.request()
            val resp = it.proceed(r)
            Log.i(NET_TAG, resp.toString())
            resp
        }.build()
        retrofit = Retrofit.Builder().baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .client(client)
            .build().create(Dao::class.java)
    }


    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(
                loginRepository = LoginRepository(
                    dataSource = LoginDataSourceImpl(dataSource)
                )
            ) as T
        }
        if (modelClass.isAssignableFrom(TestViewModel::class.java)) {
            return TestViewModel(
                repository = QuestionnaireRepository(
                    dataSource = dataSource
                )
            ) as T
        }
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(
                repository = QuestionnaireRepository(
                    dataSource = dataSource
                )
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
