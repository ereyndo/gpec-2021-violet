package com.example.violet_team.data.datasources

import com.example.violet_team.data.model.Emotion
import com.example.violet_team.data.model.Question

interface QuestionnaireDataSource {
    suspend fun getQuestions() : List<Question>
    fun getMoodFeelingHistory() : Map<Emotion, Int>

    fun postMoodFeelingsTest(emotions: List<Emotion>)

}