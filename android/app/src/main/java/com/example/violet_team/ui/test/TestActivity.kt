package com.example.violet_team.ui.test

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.violet_team.databinding.ActivityTestBinding

class TestActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTestBinding
    //private val testViewModel by viewModels<TestViewModel> { ViewModelsFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTestBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }




}