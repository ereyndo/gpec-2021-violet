package com.example.violet_team.data.model

data class Answer(val id :Int, val emotion: Emotion, val text : String)
