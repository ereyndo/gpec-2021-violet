package com.example.violet_team.ui.test

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.violet_team.R
import com.example.violet_team.databinding.FragmentTestBinding
import com.example.violet_team.ui.ViewModelsFactory


class TestFragment : Fragment() {
    private val testViewModel by activityViewModels<TestViewModel> { ViewModelsFactory }
    private lateinit var binding: FragmentTestBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTestBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (savedInstanceState == null) initialiseView()
    }

    private fun initialiseView() {

        val q = testViewModel.getNextQuestion()
        if (q == null){
            goToResultsFragment()
            return
        }
        binding.question.text = q.text
        q.answers.forEach { answer ->
            val btnTag = Button(activity)
            btnTag.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            btnTag.text = answer.text
            btnTag.setOnClickListener {
                testViewModel.addPoint(answer.emotion)
                binding.buttonLayout.removeAllViews()
                initialiseView()
            }
            binding.buttonLayout.addView(btnTag)
        }

    }


    private fun goToResultsFragment() {
        parentFragmentManager
            .beginTransaction()
            .setReorderingAllowed(true)
            .replace(R.id.fragment_container_test,TestResultsFragment())
            .addToBackStack(null)
            .commit()
    }

}