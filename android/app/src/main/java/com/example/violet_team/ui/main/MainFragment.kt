package com.example.violet_team.ui.main

import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.violet_team.R
import com.example.violet_team.data.model.Emoji
import com.example.violet_team.data.model.Emotion
import com.example.violet_team.databinding.FragmentMainBinding
import com.example.violet_team.ui.ViewModelsFactory
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding
    private val mainViewModel by activityViewModels<MainViewModel> { ViewModelsFactory }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val buttons = initialiseFastTestTable()

        binding.btnSubmit.setOnClickListener { button ->
            mainViewModel.saveMoodFeelingsTest(binding.editText.text.toString())
            buttons.forEach { it.background = null }
            binding.editText.setText("")
            button.isEnabled = false
        }
        mainViewModel.emotionHistory.observe(this)
        {
            initialisePieChart(it)
        }
        mainViewModel.currentEmotions.observe(this) {
            binding.btnSubmit.isEnabled = it.isNotEmpty()
        }
    }

    private fun initialisePieChart(emotionHistory : Map<Emotion,Int>) {
        if (emotionHistory.values.sum() < 5) return

        val pieEntries = mutableListOf<PieEntry>()
        val label = ""
        val data = emotionHistory.mapKeys { it.key.name }
        val colors = listOf(
            Color.parseColor("#304567"),
            Color.parseColor("#309967"),
            Color.parseColor("#476567"),
            Color.parseColor("#890567"),
            Color.parseColor("#a35567"),
            Color.parseColor("#ff5f67"),
            Color.parseColor("#3ca567")
        )

        data.forEach {
            pieEntries.add(PieEntry(it.value.toFloat(), it.key))
        }

        val pieDataSet = PieDataSet(pieEntries, label).apply {
            valueTextSize = 12f
            setColors(colors.take(pieEntries.size))
        }
        val pieData = PieData(pieDataSet).apply {
            setDrawValues(true)
            setValueFormatter(PercentFormatter())
        }
        binding.piechart.data = pieData
        binding.piechart.apply {
            setUsePercentValues(true)
            description.isEnabled = false
            isRotationEnabled = true
            dragDecelerationFrictionCoef = 0.9f
            rotationAngle = 0f
            animateY(1400, Easing.EasingOption.EaseInOutQuad)

            invalidate()
        }
    }


    private fun initialiseFastTestTable(): List<TextView> {
        val columns = 4
        val rowsList = Emotion.values().toList().chunked(columns)

        val buttons = mutableListOf<TextView>()
        rowsList.forEachIndexed { row, rowList ->
            val tableRow = TableRow(activity).apply {
                layoutParams = TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT
                ).apply { setMargins(2, 2, 2, 2) }
                setHorizontalGravity(Gravity.CENTER)
            }

            rowList.forEachIndexed { column, type ->
                val textView = TextView(activity).apply {
                    text = Emoji.valueOf(type.name).emoji
                    setTextSize(TypedValue.COMPLEX_UNIT_SP, 62f)
                    setOnClickListener {
                        if (background != null) {
                            mainViewModel.removeCurrentEmotion(type)
                            background = null
                        } else {
                            mainViewModel.addCurrentEmotion(type)
                            setBackgroundColor(resources.getColor(R.color.green_200))
                        }
                    }
                }
                buttons.add(textView)
                tableRow.addView(textView, column)
            }
            binding.tlFastTest.addView(tableRow, row)
        }
        return buttons
    }

}

/*  val client = OkHttpClient.Builder().addInterceptor {
      val r = it.request()
      val resp = it.proceed(r)
      Log.i("NET", resp.toString())
      resp
  }.build()
  val r = Retrofit.Builder()
      //.addConverterFactory( NullOnEmptyConverterFactory())
      .addConverterFactory(GsonConverterFactory.create())
      .client(client)
      .baseUrl("http://10.0.2.2:8080")
      .build().create(Dao::class.java)

  GlobalScope.launch {
      try{
          //r.register(User("un 5mi w l", "asd", "u q m s 75e  r"))
      }catch (ex : EOFException){
          Log.i("NET", "post response")
      }

      try {
          val q = r.getQuestionss()
          Log.i("NET", "onViewCreated: $q")
      }catch (ex: Exception){
          Log.e("NET", "onViewCreated: EXCEPTION: ${ex.message}", )
      }*/