package com.example.violet_team.retrofit

import com.example.violet_team.data.datasources.LoginDataSource
import com.example.violet_team.data.datasources.QuestionnaireDataSource
import com.example.violet_team.data.model.Question
import com.example.violet_team.data.model.QuestionServer
import com.example.violet_team.data.model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface Dao : QuestionnaireDataSource, LoginDataSource {
    @GET("api/questions")
    override suspend fun getQuestions(): List<Question>

    @GET("api/questions")
    suspend fun getQuestionss(): List<QuestionServer>

    @POST("login")
    suspend fun login(@Body user: User)

    @POST("register")
    suspend fun register(@Body user: User)
}