package com.example.violet_team.data.model

data class Question(val id: Int, val answers: List<Answer>, val text: String)