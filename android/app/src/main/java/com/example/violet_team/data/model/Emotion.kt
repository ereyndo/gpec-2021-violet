package com.example.violet_team.data.model

enum class Emotion(i: Int) {
    ANGER(1),
    FEAR(2),
    TRUST(3),
    JOY(4),
    SADNESS(5),
    SURPRISE(6),
    DISGUST(7),
    ANTICIPATION(8)
}