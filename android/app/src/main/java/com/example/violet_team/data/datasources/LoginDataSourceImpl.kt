package com.example.violet_team.data.datasources

import android.util.Log
import com.example.violet_team.data.NET_TAG
import com.example.violet_team.data.Result
import com.example.violet_team.data.model.User
import com.example.violet_team.retrofit.Dao
import java.io.EOFException
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSourceImpl(private val dao: LoginDataSource) {

    suspend fun login(email: String, password: String): Result<User> {
        val user = User(email, password, "")
        return try {
            //dao.login(user)
            Result.Success(user)
        } catch (ex: EOFException) {
            Log.i(NET_TAG, "post response")
            Result.Success(user)
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging", e))
        }
    }

    suspend fun register(email: String, password: String): Result<User> {
        val user = User(email, password, "")
        return try {
            //dao.register(user)
            Result.Success(user)
        } catch (ex: EOFException) {
            Log.i(NET_TAG, "post response")
            Result.Success(user)
        } catch (e: Throwable) {
            Result.Error(IOException("Error register", e))
        }
    }

    fun logout() {

        // TODO: revoke authentication
    }
}