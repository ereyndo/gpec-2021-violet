package com.example.violet_team.ui.test

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.violet_team.databinding.FragmentTestResultsBinding
import com.example.violet_team.ui.ViewModelsFactory
import com.example.violet_team.ui.main.MainActivity
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter


class TestResultsFragment : Fragment() {
    private val testViewModel by activityViewModels<TestViewModel> { ViewModelsFactory }
    private lateinit var binding: FragmentTestResultsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTestResultsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnEnd.setOnClickListener {
            startActivity(
                Intent(
                    activity,
                    MainActivity::class.java
                )
            )
        }

        var index = 0
        val barEntries =
            testViewModel.getPoints()
                .mapValues { it.value / testViewModel.getPointsAmount(it.key).toFloat() * 100 }
                .map { BarEntry(index++.toFloat(),it.value) }
        val emotions = testViewModel.getPoints().keys.map { it.name }
        val barDataSet = BarDataSet(barEntries, "Emotions").apply {
            setDrawValues(false)
        }
        barDataSet.colors = colors.take(barEntries.size)

        binding.barchart.apply {

            setDrawGridBackground(false)
            xAxis.setDrawGridLines(false)
            legend.isEnabled = false
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.granularity = 1f
            xAxis.valueFormatter = IAxisValueFormatter { value, axis -> emotions[value.toInt()] }
            axisRight.isEnabled = false
            description = Description().apply { isEnabled = false }
            legend.form = Legend.LegendForm.LINE
            legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
            legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
            legend.orientation = Legend.LegendOrientation.HORIZONTAL
            axisLeft.axisMaximum = 100f
            axisLeft.axisMinimum = 0f
            //axisLeft.setDrawGridLines(false)
            data = BarData(barDataSet)
            invalidate()

        }

    }

    companion object {
        val colors = listOf(
            Color.parseColor("#304567"),
            Color.parseColor("#309967"),
            Color.parseColor("#476567"),
            Color.parseColor("#890567"),
            Color.parseColor("#a35567"),
            Color.parseColor("#ff5f67"),
            Color.parseColor("#3ca567")
        )
    }
}