# Requirements to run
AndroidStudio
# How to run
### First method
1. In AndroidStudio Build Bundle(s) / APK(s) > Build APK(s)
2. Install app-debug.apk from "gpec-2021-violet\android\app\build\outputs\apk\debug" on mobile phone
   with Android (API>=21)
### Second method
1. In AndroidStudio AVD Manager configure emulator device or plug in mobile phone
   with Android (API>=21) and enabled developer mode
2. Run > Run 'app'