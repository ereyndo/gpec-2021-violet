import {Injectable} from '@angular/core';
import {Answer, Question} from '../models/interfaces';
import {Router} from '@angular/router';
import {HttpService} from '@shared/services/http.service';
import {points} from '@shared/models/types';
import {AsyncSubject, Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuestionsServiceService {
  givenAnswers: number;
  formCompleted: boolean;
  needResults: boolean;
  questions$ = new AsyncSubject<Question[]>();
  questions?: Question[];
  points: points;

  constructor(
    private router: Router,
    private httpService: HttpService,
  ) {
    this.givenAnswers = 0;
    this.points = this.setDefaultPoints();
    this.formCompleted = false;
    this.needResults = false;
    this.httpService.getQuestions().subscribe(
      questions => {
        this.questions = questions;
        this.questions$.next(this.questions);
        this.questions$.complete();
      },
      error => {
        console.log(error)
      }
    );
  }

  onAnswerClick(answer: Answer): void {
    this.givenAnswers++;
    answer.emotionCorrelations.forEach(ans => {
      for (let i = 0; i < this.points.length; i++) {
        if (ans.emotion === this.points[i][0]) {
          this.points[i][1] += ans.points;
        }
      }
    })
    if (this.questions?.length === this.givenAnswers) {
      this.formCompleted = true;
    }
  }

  onResetClick(): void {
    this.points = this.setDefaultPoints();
    this.formCompleted = false;
    this.needResults = false;
    this.givenAnswers = 0;
  }

  onResultsClick(): void {
    this.needResults = true;
  }

  setDefaultPoints(): any {
    let newArray: points = [
      ['ANGER', 0],
      ['FEAR', 0],
      ['TRUST', 0],
      ['JOY', 0],
      ['SADNESS', 0],
      ['SURPRISE', 0],
      ['DISGUST', 0],
      ['ANTICIPATION', 0],
    ];
    return newArray;
  }

  getPoints() {
    return this.points;
  }

  getCompleteness(): boolean {
    return this.formCompleted;
  }

  getQuestionsObservable(): AsyncSubject<Question[]> {
    return this.questions$;
  }

  checkDroppedAnswers(): boolean {
    return !this.givenAnswers;
  }
}
