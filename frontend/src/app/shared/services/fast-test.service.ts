import {Injectable} from '@angular/core';
import {Emoji, EmotionFeelingData, Question, Result} from '@shared/models/interfaces';
import {AsyncSubject} from 'rxjs';
import {HttpService} from '@shared/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class FastTestService {
  private emojis: Emoji[];
  private results: Result[] = [];
  private amountOfAnswers: number = 0;
  private history: EmotionFeelingData[];
  private history$ = new AsyncSubject<EmotionFeelingData[]>();

  constructor(
    private httpService: HttpService
  ) {
    this.setEmojis();
    this.setHistory();
  }

  setEmojis(): void {
    this.emojis = [
      {
        emojiTitle: 'Anger',
        emojiLink: '😠',
        chosen: false
      },
      {
        emojiTitle: 'Fear',
        emojiLink: '😰',
        chosen: false
      },
      {
        emojiTitle: 'Trust',
        emojiLink: '🤨',
        chosen: false
      },
      {
        emojiTitle: 'Joy',
        emojiLink: '😄',
        chosen: false
      },
      {
        emojiTitle: 'Sadness',
        emojiLink: '😞',
        chosen: false
      },
      {
        emojiTitle: 'Surprise',
        emojiLink: '😮',
        chosen: false
      },
      {
        emojiTitle: 'Disgust',
        emojiLink: '🤢',
        chosen: false
      },
      {
        emojiTitle: 'Anticipation',
        emojiLink: '😒',
        chosen: false
      }
    ]
  }

  getEmojis(): Emoji[] {
    return this.emojis;
  }

  initResults(): void {
    this.results = [];
    let emotions = [
      'Anger',
      'Fear',
      'Trust',
      'Joy',
      'Sadness',
      'Surprise',
      'Disgust',
      'Anticipation'
    ];
    emotions.map(emotion => {
      this.results.push({
        emotion,
        score: 0,
        width: 0
      });
    });
  }

  setResults(): void {
    let maxValue: number = 1;
    this.history.map(el => {
      el.emotions.map(emotion => {
        this.results = this.results.map(result => {
          if (emotion === result.emotion.toUpperCase()) {
            result.score++;
            if (result.score > maxValue) {
              maxValue = result.score;
            }
          }
          return result;
        })
      });
    });
    this.results = this.results.map(result => {
      result.width = result.score / maxValue * 100;
      return result;
    });
  }

  getResults(): Result[] {
    return this.results;
  }

  getAmountOfAnswers(): number {
    return this.amountOfAnswers;
  }

  setHistory(): void {
    this.httpService.getEmotionsFeelingHistory().subscribe(
      history => {
        this.history = history;
        this.amountOfAnswers = history.length;
        this.initResults();
        this.setResults();
        this.history$.next(this.history);
        this.history$.complete();
      },
      error => {
        console.log(error)
      }
    );
  }

  getHistoryObservable(): AsyncSubject<EmotionFeelingData[]> {
    this.history$ = new AsyncSubject<EmotionFeelingData[]>();
    this.setHistory();
    return this.history$;
  }

  getChosenEmotions(): string[] {
    return this.emojis.reduce((acc: string[], emoji) => {
      if (emoji.chosen) {
        acc.push(emoji.emojiTitle.toUpperCase());
      }
      return acc;
    }, []);
  }

  getEmotionFeelingData(reasonOfFeeling: string): EmotionFeelingData {
    return {
      emotions: this.getChosenEmotions(),
      reasonOfFeeling
    };
  }

  onClick(clickedEmoji: Emoji): void {
    this.emojis = this.emojis.map(emoji => {
      if (emoji.emojiTitle === clickedEmoji.emojiTitle) {
        emoji.chosen = !emoji.chosen;
      }
      return emoji;
    });
  }

  checkIfSubmitEnabled(): boolean {
    return !this.emojis.find(emoji => emoji.chosen);
  }
}
