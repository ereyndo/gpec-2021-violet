import { Injectable } from '@angular/core';
import {QuestionsServiceService} from "./questions-service.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ResultsService {
  points: [[string, number], [string, number], [string, number], [string, number], [string, number], [string, number], [string, number], [string, number]] | undefined
  constructor(
    private questionsService: QuestionsServiceService,
    private router: Router
    ) { }

  getPoints() {
    this.points = this.questionsService.getPoints();
    let maxValue = 0;
    this.points.forEach(el => {
      if(el[1] > maxValue) {
        maxValue = el[1];
      }
    })
    let newPoints: any = [];
    this.points.forEach(el => {
      let text = el[0];
      let score = el[1]/maxValue*100;
      newPoints.push([text, score]);
    })
    return newPoints;
  }

  onRetakeQuestionsClick(): void {

  }

}
