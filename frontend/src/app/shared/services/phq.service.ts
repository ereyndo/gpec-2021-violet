import { Injectable } from '@angular/core';
import {PHQAnswer, PHQQuestion} from "@shared/models/interfaces";
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PhqService {
  questions: PHQQuestion[];
  depressionCounter = 0;
  phqResults = new BehaviorSubject(0);
  showTest = new BehaviorSubject(true);
  guide = new BehaviorSubject('');
  constructor() {
    this.setQuestions();
  }

  getQuestions(): PHQQuestion[] {
    return this.questions;
  }

  setQuestions(): void {
    this.questions = [
      {
        text: 'Little interest or pleasure in doing things?',
        number: 1,
        show: true,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Feeling down, depressed, or hopeless?',
        number: 2,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Trouble falling or staying asleep, or sleeping too much?',
        number: 3,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Feeling tired or having little energy?',
        number: 4,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Poor appetite or overeating?',
        number: 5,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Feeling bad about yourself — or that you are a failure or have let yourself or your family down?',
        number: 6,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Trouble concentrating on things, such as reading the newspaper or watching television?',
        number: 7,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Moving or speaking so slowly that other people could have noticed? ' +
          'Or so fidgety or restless that you have been moving a lot more than usual?',
        number: 8,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
      {
        text: 'Thoughts that you would be better off dead, or thoughts of hurting yourself in some way?',
        number: 9,
        show: false,
        answers: [
          {
            text: 'Not at all',
            points: 0
          },
          {
            text: 'Several days',
            points: 1
          },
          {
            text: 'More then half the days',
            points: 2
          },
          {
            text: 'Nearly every day',
            points: 3
          },
        ]
      },
    ]
  }

  answerClicked(answer: PHQAnswer, question: PHQQuestion): void {
    this.depressionCounter+= answer.points;
    if(question.number === 9) {
      this.phqResults.next(this.depressionCounter);
      this.showTest.next(false);
      this.chooseGuide();
      this.setQuestions();
    } else {
      for(let i = 0; i < this.questions.length; i++) {
        if(this.questions[i].number === question.number) {
          this.questions[i].show = false;
          this.questions[i+1].show = true;
        }
      }
    }
  }

  retakeTest() {
    this.setQuestions();
    this.phqResults.next(0);
    this.showTest.next(true);
    this.guide.next('');
  }

  chooseGuide() {
    let res = 0;
    this.phqResults.subscribe(val => res = +val);
    if(res <= 4) {
      this.guide.next('Scores ≤4 suggest minimal depression which may not require treatment.');
    } else if(res > 4 && res < 10) {
      this.guide.next('Scores 5-9 suggest mild depression which may require only watchful' +
        ' waiting and repeated PHQ-9 at followup.');
    } else if(res > 9 && res < 15) {
      this.guide.next('Scores 10-14 suggest moderate depression severity;' +
        ' patients should have a treatment plan ranging form counseling, followup, and/or pharmacotherapy.');
    } else if(res > 14 && res < 20) {
      this.guide.next('Scores 15-19 suggest moderately severe depression; ' +
        'patients typically should have immediate initiation of pharmacotherapy and/or psychotherapy. ' +
        '\n \nWARNING: This patient is having thoughts concerning for suicidal ideation or self-harm, ' +
        'and should be probed further, referred, or transferred ' +
        'for emergency psychiatric evaluation as clinically appropriate ' +
        'and depending on clinician overall risk assessment.');
    } else if(res > 19) {
      this.guide.next('Scores 20 and greater suggest severe depression; patients typically ' +
        'should have immediate initiation of pharmacotherapy and expedited referral to mental health specialist.' +
        '\n \nWARNING: This patient is having thoughts concerning for suicidal ideation or self-harm, ' +
        'and should be probed further, referred, or transferred ' +
        'for emergency psychiatric evaluation as clinically appropriate ' +
        'and depending on clinician overall risk assessment.')
    }
  }
}
