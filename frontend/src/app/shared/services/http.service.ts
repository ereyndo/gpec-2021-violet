import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {EmotionFeelingData, Question} from '@shared/models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) {
  }

  getQuestions(): Observable<Question[]> {
    return this.http.get<Question[]>('https://psychological-service.herokuapp.com/questions', {withCredentials: true});
  }

  postFastEmotionFeelingData(emotionFeelingData: EmotionFeelingData): Observable<any> {
    return this.http.post('https://psychological-service.herokuapp.com/moodfeelingtest', emotionFeelingData, {withCredentials: true});
  }

  getEmotionsFeelingHistory(): Observable<EmotionFeelingData[]> {
    return this.http.get<EmotionFeelingData[]>('https://psychological-service.herokuapp.com/moodfeelinghistory', {withCredentials: true});
  }

}
