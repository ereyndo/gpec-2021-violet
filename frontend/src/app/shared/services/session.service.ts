import { Injectable } from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private loggedIn: boolean;

  constructor(private router: Router) {
    this.loggedIn = !!localStorage.getItem('email');
  }

  public isLoggedIn(): boolean {
    return this.loggedIn;
  }

  public setLoggedIn(value: boolean): void {
    const previous = this.loggedIn;
    this.loggedIn = value;
    if (previous === this.loggedIn) {
      return;
    }
    const i = this.router.config.findIndex(x => x.path === '');
    this.router.config.splice(i, 1);
    this.router.config.push(
      {
        path: '',
        loadChildren: () => import('@main/main.module').then(m => m.MainModule),
      }
    );
  }
}
