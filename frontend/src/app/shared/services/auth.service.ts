import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {createFormData} from '@shared/functions/createFormData';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(email: string, password: string): Observable<any> {
    const form = createFormData({email, password});
    return this.http.post('https://psychological-service.herokuapp.com/login', form, {withCredentials: true});
  }

  register(username: string, email: string, password: string): Observable<any> {
    return this.http.post('https://psychological-service.herokuapp.com/register', {username, email, password}, {withCredentials: true});
  }

  logout(): Observable<any> {
    return this.http.post('https://psychological-service.herokuapp.com/logout', {}, {withCredentials: true});
  }
}
