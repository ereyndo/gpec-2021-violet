export interface Login {
  email: string;
  password: string;
}

export interface Signup {
  username: string;
  email: string;
  password: string;
}

export interface Question {
  text: string,
  answers: Answer[]
}

export interface Answer {
  text: string,
  emotionCorrelations: [
    {
      emotion: string,
      points: number
    }
  ],
  chosen: boolean
}

export interface PHQQuestion {
  text: string,
  number: number,
  show: boolean,
  answers: PHQAnswer[]
}

export interface PHQAnswer {
  text: string,
  points: number
}

export interface Emoji {
  emojiTitle: string,
  emojiLink: string,
  chosen: boolean
}

export interface EmotionFeelingData {
  emotions: string[],
  reasonOfFeeling: string
}

export interface Result {
  emotion: string,
  score: number,
  width: number
}
