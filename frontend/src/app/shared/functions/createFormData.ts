export const createFormData = (object: Object): FormData => {
  const form = new FormData();
  for (const [key, value] of Object.entries(object)) {
    form.append(key, value);
  }
  return form;
};
