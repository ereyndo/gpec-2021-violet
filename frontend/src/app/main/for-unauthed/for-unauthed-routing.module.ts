import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FrontPageComponent} from '@main/for-unauthed/front-page/front-page.component';
import {LoginPageComponent} from '@main/for-unauthed/login-page/login-page.component';
import {SignupPageComponent} from '@main/for-unauthed/signup-page/signup-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FrontPageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: 'register',
    component: SignupPageComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForUnauthedRoutingModule { }
