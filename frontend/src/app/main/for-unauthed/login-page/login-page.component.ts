import {Component, OnInit} from '@angular/core';
import {Login} from '@shared/models/interfaces';
import {AuthService} from '@shared/services/auth.service';
import {Router} from '@angular/router';
import {SessionService} from '@shared/services/session.service';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  loginFailed: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private sessionService: SessionService
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(
    passedValues: Login
  ): void {
    this.authService.login(
      passedValues.email,
      passedValues.password
    ).subscribe(
      () => {
        localStorage.setItem('email', passedValues.email);
        this.sessionService.setLoggedIn(true);
        this.router.navigate(['/']);
      },
      () => {
        this.loginFailed = true;
        setTimeout(() => {
          this.loginFailed = false;
        }, 2500);
      }
    );
  }

}
