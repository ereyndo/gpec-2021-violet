import {Component, OnInit} from '@angular/core';
import {Signup} from '@shared/models/interfaces';
import {AuthService} from '@shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {

  signupFailed: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onSubmit(
    passedValues: Signup
  ): void {
    this.authService.register(
      passedValues.username,
      passedValues.email,
      passedValues.password,
    ).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      () => {
        this.signupFailed = true;
        setTimeout(() => {
          this.signupFailed = false;
        }, 2500);
      }
    );
  }

}
