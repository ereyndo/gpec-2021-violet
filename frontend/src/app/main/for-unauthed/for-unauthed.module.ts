import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FrontPageComponent} from './front-page/front-page.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {SignupPageComponent} from './signup-page/signup-page.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { ForUnauthedRoutingModule } from './for-unauthed-routing.module';

@NgModule({
  declarations: [
    FrontPageComponent,
    LoginPageComponent,
    SignupPageComponent,
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    ForUnauthedRoutingModule,
  ]
})
export class ForUnauthedModule {
}
