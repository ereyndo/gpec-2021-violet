import {Component, DoCheck, OnInit} from '@angular/core';
import {FastTestService} from "@shared/services/fast-test.service";
import {Emoji, EmotionFeelingData, Result} from '@shared/models/interfaces';
import {HttpService} from '@shared/services/http.service';

@Component({
  selector: 'app-fast-test',
  templateUrl: './fast-test.component.html',
  styleUrls: ['./fast-test.component.scss']
})
export class FastTestComponent implements OnInit, DoCheck {
  emojis: Emoji[];
  inputVal = '';
  history: EmotionFeelingData[];
  results: Result[];

  constructor(
    public fastTestsService: FastTestService,
    private httpService: HttpService
  ) {
  }

  ngOnInit(): void {
  }

  ngDoCheck() {
    this.emojis = this.fastTestsService.getEmojis();
    this.results = this.fastTestsService.getResults();
  }

  submitTest() {
    this.httpService.postFastEmotionFeelingData(this.fastTestsService.getEmotionFeelingData(this.inputVal)).subscribe(
      () => {
        this.getHistory();
        this.inputVal = '';
        this.fastTestsService.setEmojis();
      },
      error => {
        console.log(error);
      }
    )
  }

  getHistory() {
    this.fastTestsService.getHistoryObservable().subscribe(
      history => {
        this.history = history;
      },
      error => {
        console.log(error)
      }
    );
  }
}
