import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Answer, Question} from '@shared/models/interfaces';
import {QuestionsServiceService} from '@shared/services/questions-service.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {
  @Input() question: Question | undefined;
  @Input() qIndex: number
  @Output() answerClickEmitter: EventEmitter<Answer> = new EventEmitter<Answer>();
  disabled = false;

  constructor(public questionService: QuestionsServiceService) { }

  ngDoCheck(): void {
    if (this.questionService.checkDroppedAnswers()) {
      this.disabled = false;
    }
  }

  onAnswerClick(answer: Answer) {
    this.disabled = true;
    this.answerClickEmitter.emit(answer);
  }
}
