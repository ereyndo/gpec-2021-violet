import {Component, OnInit} from '@angular/core';
import {ResultsService} from '@shared/services/results.service';
import {QuestionsServiceService} from '@shared/services/questions-service.service';
import {points} from '@shared/models/types';

@Component({
  selector: 'app-questions-form-result',
  templateUrl: './questions-form-result.component.html',
  styleUrls: ['./questions-form-result.component.scss']
})
export class QuestionsFormResultComponent implements OnInit {

  public points: points | undefined

  constructor(
    private resultsService: ResultsService,
    public questionsService: QuestionsServiceService
  ) {
  }

  ngOnInit(): void {
    this.points = this.resultsService.getPoints();
  }

}
