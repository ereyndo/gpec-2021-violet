import {Directive, ElementRef, HostListener, Renderer2} from '@angular/core';
import {QuestionsServiceService} from '@shared/services/questions-service.service';

@Directive({
  selector: '[appChosen]'
})
export class ChosenDirective {

  constructor(
    private elRef: ElementRef,
    private renderer: Renderer2,
    private questionService: QuestionsServiceService
  ) {
  }

  ngDoCheck() {
    if (this.questionService.checkDroppedAnswers()) {
      this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', defaultStatus);
    }
  }

  @HostListener('click') setGreen(): void {
    this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'rgba(0,151,0, 0.3)');
  }
}
