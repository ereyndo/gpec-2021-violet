import {Component, DoCheck, OnInit} from '@angular/core';
import {QuestionsServiceService} from '@shared/services/questions-service.service';
import {Question} from '@shared/models/interfaces';

@Component({
  selector: 'app-questions-form',
  templateUrl: './questions-form.component.html',
  styleUrls: ['./questions-form.component.scss']
})
export class QuestionsFormComponent implements OnInit, DoCheck {
  questions?: Question[];
  formComplete: boolean | undefined;
  constructor(
    public questionsService: QuestionsServiceService
  ) {
  }

  ngOnInit(): void {
    this.questionsService.getQuestionsObservable().subscribe(
      questions => {
        this.questions = questions;
      },
      error => {
        console.log(error)
      }
    );
  }

  ngDoCheck(): void {
    this.formComplete = this.questionsService.getCompleteness();
  }

  onResetClick() {
    this.questionsService.onResetClick();
  }
}
