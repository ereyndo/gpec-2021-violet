import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
  showThanks = false;
  form: FormGroup;
  date: string;
  time: string;
  constructor() {
  }

  ngOnInit(): void {
    this.setForm();
  }

  submit(): void {
    this.date = this.form.value.date;
    this.time = this.form.value.time;
    this.showThanks = true;
    this.form.reset();
    setTimeout(() => this.showThanks = false, 2500);
  }

  setForm(): void {
    this.form = new FormGroup(
      {
        date: new FormControl(''),
        time: new FormControl('')
      }
    )
  }
}
