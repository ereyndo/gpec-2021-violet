import {Component, DoCheck, OnInit} from '@angular/core';
import {PhqService} from "@shared/services/phq.service";
import {PHQAnswer, PHQQuestion} from "@shared/models/interfaces";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-phq',
  templateUrl: './phq.component.html',
  styleUrls: ['./phq.component.scss']
})
export class PhqComponent implements OnInit {
  phqQuestions: PHQQuestion[];
  showTest: boolean;
  testResults: number;
  showTestSub: Subscription;
  testResultsSub: Subscription;
  guide: string;
  guideSub: Subscription;
  constructor(private phqService: PhqService) { }

  ngOnInit(): void {
    this.phqQuestions = this.phqService.getQuestions();
    this.showTestSub = this.phqService.showTest.subscribe(flag => this.showTest = flag);
    this.testResultsSub = this.phqService.phqResults.subscribe(val => this.testResults = val);
    this.guideSub = this.phqService.guide.subscribe(val => this.guide = val);
  }

  answerClicked(answer: PHQAnswer, question: PHQQuestion) {
    this.phqService.answerClicked(answer, question);
    this.phqQuestions = this.phqService.getQuestions();
  }

  retakeTest() {
    this.phqService.retakeTest();
    this.phqService.getQuestions();
  }
}
