import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainpageComponent} from './mainpage.component';
import {QuestionComponent} from './questions-form/question/question.component';
import {QuestionsFormResultComponent} from './questions-form/questions-form-result/questions-form-result.component';
import {FastTestComponent} from './fast-test/fast-test.component';
import {QuestionsFormComponent} from './questions-form/questions-form.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainpageRoutingModule} from './mainpage-routing.module';
import {ChosenDirective} from '@main/mainpage/questions-form/chosen.directive';
import { PhqComponent } from './phq/phq.component';
import { GuideComponent } from './guide/guide.component';
import { EmotionBaseComponent } from './emotion-base/emotion-base.component';
import { AppointmentComponent } from './appointment/appointment.component';

@NgModule({
  declarations: [
    MainpageComponent,
    QuestionsFormComponent,
    QuestionComponent,
    FastTestComponent,
    QuestionsFormResultComponent,
    ChosenDirective,
    PhqComponent,
    GuideComponent,
    EmotionBaseComponent,
    AppointmentComponent
  ],
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule,
        MainpageRoutingModule,
        ReactiveFormsModule,
    ]
})
export class MainpageModule {
}
