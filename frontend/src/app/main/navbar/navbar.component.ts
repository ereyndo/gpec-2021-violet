import {Component, DoCheck, Injector} from '@angular/core';
import {QuestionsServiceService} from '@shared/services/questions-service.service';
import {Router} from '@angular/router';
import {SessionService} from '@shared/services/session.service';
import {AuthService} from '@shared/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements DoCheck {
  email: string | null = '';
  isLoggedIN: boolean;

  questionsService?: QuestionsServiceService = undefined;

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private injector: Injector,
    private authService: AuthService
  ) {
    if (this.sessionService.isLoggedIn()) {
      this.questionsService =  <QuestionsServiceService>this.injector.get(QuestionsServiceService);
    }
  }

  ngDoCheck() {
    this.email = localStorage.getItem('email');
    this.isLoggedIN = !!this.email;
  }

  logOut() {
    this.authService.logout().subscribe(
      () => {
        localStorage.removeItem('email');
        this.sessionService.setLoggedIn(false);
        this.router.navigate(['/']);
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
