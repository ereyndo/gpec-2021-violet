import {NgModule} from '@angular/core';
import {Routes, ROUTES} from '@angular/router';
import {SessionService} from '@shared/services/session.service';

@NgModule({
  declarations: [],
  imports: [],
  providers: [
    {
      provide: ROUTES,
      useFactory: configHandlerRoutes,
      deps: [SessionService],
      multi: true
    }
  ]
})
export class MainModule {
}

export function configHandlerRoutes(sessionService: SessionService) {
  let routes: Routes = [];
  if (sessionService.isLoggedIn()) {
    routes = [
      {
        path: '',
        loadChildren: () => import('@main/mainpage/mainpage.module').then(m => m.MainpageModule),
      }
    ];
  } else {
    routes = [
      {
        path: '',
        loadChildren: () => import('@main/for-unauthed/for-unauthed.module').then(m => m.ForUnauthedModule),
      }
    ];
  }
  return routes;
}
