# Git commands
## Pull project
git clone git@gitbud.epam.com:epm-ace/gpec-2021-violet.git

## Create and change to new branch
git branch <branch_name>

git checkout <branch_name>

## Push branch
git add .

git commit -m "message"

git push -u origin <branch_name>

## Revise changes made since the last commit
git checkout <branch_name> -f

## Update branch with the newest version of main

git checkout main
	
git pull

git checkout <branch_name>

git merge main

Note that updating a branch is most of the time unnecessary.

# If you want your branch to be merged, please create a merge request for it.
## Every branch should be merged into main.

# Information about the backend, frontend, and android available in their respective folder inside the README.md file.
